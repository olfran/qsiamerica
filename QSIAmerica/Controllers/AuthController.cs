﻿using QSIAmerica.Lib;
using QSIAmerica.Lib.Authentication;
using QSIAmerica.Lib.Services;
using QSIAmerica.Models;
using System.Web.Mvc;

namespace QSIAmerica.Controllers
{
    public class AuthController : BaseController
    {
        readonly IAuthentication _authentication;
        readonly IConfigQSIAmerica _config;
        readonly ITokenService _tokenService;

        public AuthController(IAuthentication auth, ITokenService tokenService, IConfigQSIAmerica config)
        {
            _authentication = auth;
            _config = config;
            _tokenService = tokenService;
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            var loginResult = new LoginResult { Valid = ModelState.IsValid };

            if (ModelState.IsValid)
            {
                var authResult = _authentication.Login(model.Username, model.Password);

                if (authResult.Error)
                {
                    ModelState.AddModelError("Password", "Nombre de usuario o contraseña inválida");
                    loginResult.Valid = false;
                }
                else
                {
                    //Todo válido
                    loginResult.Username = authResult.NombreUsuario;
                    loginResult.UserId = authResult.IdUsuario;
                    loginResult.CatalogoId = authResult.IdCatalogo;
                    loginResult.RemoteIP = Request.UserHostAddress;
                    loginResult.Token = _tokenService.Generate(loginResult.UserId, loginResult.Username, loginResult.RemoteIP);
                    _tokenService.Save(loginResult.Username, loginResult.Token);
                    loginResult.Message = string.Format("Token generado exitosamente, el token tiene una validez de {0} minutos", _config.DuracionToken);

                    return Json(loginResult);
                }
            }

            foreach (var item in ModelState)
            {
                loginResult.ErrorMessages.Add(item.Key, item.Value.Errors);
            }

            return Json(loginResult);
        }

        [HttpPost]
        public ActionResult Valid(string token)
        {
            return Json(new { Error = !_tokenService.IsValidToken(token, Request.UserHostAddress) });
        }

        [HttpPost]
        public ActionResult Logout(string token)
        {
            _tokenService.Delete(token);

            return Json(LogoutJson);
        }
    }
}