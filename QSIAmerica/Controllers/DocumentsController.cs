﻿using QSIAmerica.Lib;
using QSIAmerica.Lib.Services;
using QSIAmerica.Lib.Services.Views;
using QSIAmerica.Models;
using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QSIAmerica.Controllers
{
    public class DocumentsController : BaseController
    {
        readonly IDataAccessService _dataService;
        readonly ITokenService _tokenService;
        readonly ICatalogoDataAccess _catalogoDataAccess;
        readonly IFormularioHtmlService _htmlService;
        readonly IConfigQSIAmerica _config;

        public DocumentsController(IDataAccessService dataService, ITokenService tokenService, ICatalogoDataAccess catalogoDataAccess, IFormularioHtmlService htmlService, IConfigQSIAmerica config)
        {
            _dataService = dataService;
            _tokenService = tokenService;
            _catalogoDataAccess = catalogoDataAccess;
            _htmlService = htmlService;
            _config = config;
        }

        /// <summary>
        /// Servicio de búsqueda por campos, los campos y valores deben estar separados en la URL
        /// Ej. CAMPO_1=texto&CAMPO_2=texto2... 
        /// Retorna el resultado en json
        /// </summary>
        /// <param name="token">Token de autenticación</param>
        /// <param name="idc">ID del catálogo a consultar</param>
        /// <param name="ol">Operador lógico, AND==0 OR != 0</param>
        /// <param name="p">Número de Página</param>
        /// <returns>Json</returns>
        [HttpPost]
        public ActionResult Search(string token, long idc, int ol=0, int p = 1)
        {
            TokenModel tokenModel = _tokenService.Decode(token);

            //Verificar el acceso al catálogo
            if (!_catalogoDataAccess.TieneAcceso(tokenModel.UserId, idc))
            {
                return Json(UnauthorizedJson);
            }

            _dataService.OperadorLogicoBuscar = ol;

            //Verificar el Token
            if (_tokenService.IsValidToken(tokenModel, Request.UserHostAddress))
            {
                Paginador paginador = new Paginador(p, _config.PaginationSize);

                _dataService.SetPaginador(paginador);

                //Campos de búsqueda
                IList<Campo> camposBusqueda =
                    _dataService.GetCamposFormulario(idc, _config.TipoFormularioBusqueda);

                //Campos del formulario ResultadoBusqueda
                IList<Campo> camposResultadoBusqueda =
                    _dataService.GetCamposFormulario(idc, _config.TipoFormularioResultadoBusqueda);

                //Actualizar los campos con los datos de la petición
                //@ToDo: Dependencia de Request.Form, éste método NO ES TESTEABLE
                _htmlService.BindRequest(camposBusqueda, Request.Form);

                IList<Documento> resultadoBusqueda = new List<Documento>();

                resultadoBusqueda = _dataService.Buscar(camposBusqueda, idc);

                paginador.TotalElementos = _dataService.GetTotalBuscar(idc, camposBusqueda);

                BusquedaViewModel model = new BusquedaViewModel()
                {
                    Resultados = resultadoBusqueda,
                    Query = "",
                    Paginador = paginador
                };

                return Json(model);
            }

            return Json(InvalidTokenJson);
        }


        /// <summary>
        /// Servicio de búsqueda amplia, retorna el resultado en json
        /// </summary>
        /// <param name="token">Token de autenticación</param>
        /// <param name="q">Query a consultar</param>
        /// <param name="idc">ID del catálogo a consultar</param>
        /// <param name="p">Número de Página</param>
        /// <returns>Json</returns>
        [HttpPost]
        public ActionResult SearchFull(string token, string q, long idc, int p = 1)
        {
            TokenModel tokenModel = _tokenService.Decode(token);

            //Verificar el acceso al catálogo
            if (!_catalogoDataAccess.TieneAcceso(tokenModel.UserId, idc))
            {
                return Json(UnauthorizedJson);
            }

            //Verificar el Token
            if (_tokenService.IsValidToken(tokenModel, Request.UserHostAddress))
            {
                Paginador paginador = new Paginador(p, _config.PaginationSize);

                _dataService.SetPaginador(paginador);

                //Campos del formulario ResultadoBusqueda
                IList<Campo> camposResultadoBusqueda =
                    _dataService.GetCamposFormulario(idc, _config.TipoFormularioResultadoBusqueda);

                IList<Documento> resultadoBusqueda = new List<Documento>();

                if (!String.IsNullOrEmpty(q))
                {
                    resultadoBusqueda = _dataService.BusquedaAmplia(q, camposResultadoBusqueda, idc);
                }

                paginador.TotalElementos = _dataService.
                    GetTotalBusquedaAmplia(q, camposResultadoBusqueda, idc);

                BusquedaViewModel model = new BusquedaViewModel()
                {
                    Resultados = resultadoBusqueda,
                    Query = q,
                    Paginador = paginador
                };

                return Json(model);
            }

            return Json(InvalidTokenJson);
        }
    }
}