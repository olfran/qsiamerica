﻿/*
 * Controlador Base del proyecto para el área pública del Frontend
 * Todos los controladores del frontend pueden heredar atributos comúnes de esta clase
 * 
 * Por Olfran Jiménez <olfran@gmail.com>
 */

using System.Web.Mvc;

namespace QSIAmerica.Controllers
{
    public class BaseController : Controller
    {
        public object LogoutJson
        {
            get
            {
                return new { Error = false, Message = "Si el token era valido, entonces ya fue liberado" };
            }
        }

        public object InvalidTokenJson
        {
            get
            {
                return new { Error = true, Message = "Error, token inválido o vencido" };
            }
        }

        public object NotFoundJson
        {
            get
            {
                return new { Error = true, Message = "Error, recurso no encontrado" };
            }
        }

        public object UnauthorizedJson
        {
            get
            {
                return new { Error = true, Message = "Error, usted no tiene acceso al recurso solicitado" };
            }
        }
    }
}