﻿using QSIAmerica.Lib.Services;
using QSIAmerica.Models;
using ScavUtils.Infrastructure.DataAccess;
using System;
using System.Web.Mvc;

namespace QSIAmerica.Controllers
{
    public class AttachmentsController : BaseController
    {
        readonly ITokenService _tokenService;
        readonly ICatalogoDataAccess _catalogoDataAccess;
        readonly IAdjuntoService _adjuntoService;

        public AttachmentsController(ITokenService tokenService, ICatalogoDataAccess catalogoDataAccess, IAdjuntoService adjuntoService)
        {
            _tokenService = tokenService;
            _catalogoDataAccess = catalogoDataAccess;
            _adjuntoService = adjuntoService;
        }


        /// <summary>
        /// Encuentra un documento
        /// </summary>
        /// <param name="t">Token de autenticación, debería venir en la petición GET</param>
        /// <param name="idc">Id catálogo</param>
        /// <param name="idd">Id documento</param>
        /// <param name="ida">Id adjunto</param>
        /// <returns>Archivo o Json error</returns>
        [HttpGet]
        public ActionResult Find(string t, long idc, long idd, long ida)
        {

            TokenModel tokenModel = _tokenService.Decode(t);

            //Verificar el acceso al catálogo
            if (!_catalogoDataAccess.TieneAcceso(tokenModel.UserId, idc))
            {
                return Json(UnauthorizedJson);
            }

            var adjuntoPropiedades = _adjuntoService.FindAdjunto(idc, idd, ida);
            string fileName = adjuntoPropiedades.Nombre;
            string path = _adjuntoService.GetPathAdjunto(idc, idd, adjuntoPropiedades);

            if (!String.IsNullOrEmpty(path))
            {
                return File(path, "application/octet-stream", fileName);
            }

            Response.StatusCode = 404;

            return Content("404 - Not Found");
        }

        /// <summary>
        /// Retorna todos los adjuntos
        /// </summary>
        /// <param name="token">Token de autenticación, debería venir en la cabecera HTTP</param>
        /// <param name="idc">Id catálogo</param>
        /// <param name="idd">Id documento</param>
        /// <returns>Json</returns>
        [HttpPost]
        public ActionResult FindAll(string token, long idc, long idd)
        {
            TokenModel tokenModel = _tokenService.Decode(token);

            //Verificar el acceso al catálogo
            if (!_catalogoDataAccess.TieneAcceso(tokenModel.UserId, idc))
            {
                return Json(UnauthorizedJson);
            }

            //Verificar el Token
            if (_tokenService.IsValidToken(token, Request.UserHostAddress))
            {
                return Json(_adjuntoService.GetAdjuntos(idc, idd));
            }

            return Json(InvalidTokenJson);
        }
    }
}