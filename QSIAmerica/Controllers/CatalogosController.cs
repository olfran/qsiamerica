﻿using QSIAmerica.Lib;
using QSIAmerica.Lib.Services;
using ScavUtils.Infrastructure.DataAccess;
using System.Web.Mvc;

namespace QSIAmerica.Controllers
{
    public class CatalogosController : BaseController
    {
        readonly IDataAccessService _dataService;
        readonly ITokenService _tokenService;
        readonly ICatalogoDataAccess _catalogoDataAccess;
        readonly IConfigQSIAmerica _config;

        public CatalogosController(IDataAccessService dataService, ITokenService tokenService, ICatalogoDataAccess catalogoDataAccess, IConfigQSIAmerica config)
        {
            _tokenService = tokenService;
            _catalogoDataAccess = catalogoDataAccess;
            _dataService = dataService;
            _config = config;
        }

        /// <summary>
        /// Retorna una lista con los catálogos a los que el usuario tiene acceso a partir del ID de Usuario
        /// </summary>
        /// <param name="token">Token de autenticación</param>
        /// <returns>Json</returns>
        [HttpPost]
        public ActionResult List(string token)
        {
            //Verificar el Token
            if (_tokenService.IsValidToken(token, Request.UserHostAddress))
            {
                var loginResult = _tokenService.Decode(token);
                
                return Json(_catalogoDataAccess.GetCatalogos(loginResult.UserId));
            }

            return Json(InvalidTokenJson);
        }

        /// <summary>
        /// Retorna la lista de campos correspondientes al formulario de búsqueda avanzada o filtros
        /// </summary>
        /// <param name="token">Token de autenticación</param>
        /// <param name="idc">ID del catálogo</param>
        /// <returns>Json</returns>
        [HttpPost]
        public ActionResult SearchFilters(string token, long idc)
        {
            if (_tokenService.IsValidToken(token, Request.UserHostAddress))
            {
                return Json(_dataService.GetCamposFormulario(idc, _config.TipoFormularioBusqueda));
            }

            return Json(InvalidTokenJson);
        }
    }
}