﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;

namespace QSIAmerica.Models
{
    /// <summary>
    /// ViewModel para el resultado de la búsqueda
    /// </summary>
    public class BusquedaViewModel
    {
        public IList<Documento> Resultados { get; set; }
        public string Query { get; set; }
        public Paginador Paginador { get; set; }
    }
}