﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QSIAmerica.Models
{
    /// <summary>
    /// Clase para los campos del Token
    /// </summary>
    public class TokenModel
    {
        // UserId, Username, IP, GenerationTime
        public long UserId { get; set; }
        public string Username { get; set; }
        public string RemoteIP { get; set; }

        /// <summary>
        /// Fecha - Hora de Generación del Token
        /// </summary>
        public DateTime GenerationTime { get; set; }
    }
}