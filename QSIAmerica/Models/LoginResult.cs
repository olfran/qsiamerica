﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace QSIAmerica.Models
{
    public class LoginResult
    {
        /// <summary>
        /// Representa un resultado del login para ser retornado en Json
        /// por defecto Valid = true
        /// </summary>
        public LoginResult()
        {
            Valid = true;
            ErrorMessages = new Dictionary<string, ModelErrorCollection>();
        }

        /// <summary>
        /// Indica si el resultado es válido
        /// </summary>
        public bool Valid { get; set; }

        /// <summary>
        /// Mensajes de error
        /// </summary>
        public Dictionary<string, ModelErrorCollection> ErrorMessages { get; set; }

        /// <summary>
        /// ID de usuario
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// IP del usuario que inicia sesión
        /// </summary>
        public string RemoteIP { get; set; }

        /// <summary>
        /// Token generado, con una duración de 30 minutos por defecto
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Mensaje de información general
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// ID del último catálogo seleccionado
        /// </summary>
        public long CatalogoId { get; set; }
    }
}