﻿using System;
using System.Configuration;

namespace QSIAmerica.Lib
{
    public class ConfigQSIAmerica : IConfigQSIAmerica
    {
        public int DuracionToken { get; private set; }
        public int TipoFormularioBusqueda { get; private set; }
        public int TipoFormularioResultadoBusqueda { get; private set; }
        public int PaginationSize { get; private set; }
        public string ConnectionString { get; private set; }
        public string LogsFolder { get; private set; }

        public ConfigQSIAmerica()
        {
            DuracionToken = GetInt("DuracionToken");
            TipoFormularioBusqueda = GetInt("TipoFormularioBusqueda");
            TipoFormularioResultadoBusqueda = GetInt("TipoFormularioResultadoBusqueda");
            PaginationSize = GetInt("PaginationSize");
            ConnectionString = GetConnectionString("ConnectionString");
            LogsFolder = ConfigurationManager.AppSettings["LogsFolder"];
        }

        /// <summary>
        /// Retorna un long del web.config o 0 si hubo error
        /// </summary>
        private long GetLong(string key, int defecto = 0)
        {
            long resultado = 0;

            try
            {
                long.TryParse(ConfigurationManager.AppSettings[key], out resultado);
                return resultado;
            }
            catch (Exception)
            {
                return defecto;
            }
        }

        /// <summary>
        /// Retorna un int del web.config o 0 si hubo error
        /// </summary>
        private int GetInt(string key, int defecto = 0)
        {
            int resultado = 0;

            try
            {
                int.TryParse(ConfigurationManager.AppSettings[key], out resultado);
                return resultado;
            }
            catch (Exception)
            {
                return defecto;
            }
        }

        private string GetString(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key];
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// Retorna la cadena de conexión del archivo web.config o null si hubo error
        /// </summary>
        private string GetConnectionString(string key)
        {
            try
            {
                return ConfigurationManager.ConnectionStrings[key].ConnectionString;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}