﻿namespace QSIAmerica.Lib
{
    public interface IConfigQSIAmerica
    {
        string ConnectionString { get; }
        int PaginationSize { get; }
        int TipoFormularioBusqueda { get; }
        int DuracionToken { get; }
        int TipoFormularioResultadoBusqueda { get; }
        string LogsFolder { get; }
    }
}