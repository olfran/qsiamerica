﻿using Microsoft.Practices.Unity;
using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.DataAccess.Adapters;
using ScavUtils.Infrastructure.Models;
using ScavUtils.Validators;
using QSIAmerica.Lib.Authentication;
using QSIAmerica.Lib.Services;
using QSIAmerica.Lib.Services.Views;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using QSIAmerica.Lib.Token;
using Archicentro.Criptografia.Cifrado;
using Logging;
using Logging.Backend;

namespace QSIAmerica.Lib.DI
{
    /// <summary>
    /// Contenedor de dependencias de la aplicación, en este archivo se definen todas las dependencias
    /// de la parte web en MVC
    /// </summary>
    public class UnityResolver : IDependencyResolver
    {
        IUnityContainer _unityContainer = new UnityContainer();

        //Establecer todas las dependencias de la aplicación para que sean utilizadas por los controladores
        public UnityResolver()
        {
            DoBindings();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return _unityContainer.Resolve(serviceType);
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return _unityContainer.ResolveAll(serviceType);
            }
            catch
            {
                return new List<object>();
            }
        }

        private void DoBindings()
        {
            //Dependencias de la aplicación, como son interfaces se pueden cambiar
            //facilmente por otras implementaciones, Programming To Interfaces

            IConfigQSIAmerica config = new ConfigQSIAmerica();
            Paginador paginador = new Paginador(0, config.PaginationSize);
            ISQLCommonsScav sqlCommons = new SQLCommonsScav(config.ConnectionString, paginador);
            ITesauroDataAccess tesauroDataAccess = new TesauroDataAccess(sqlCommons);
            IScavAdapter scavAdapter = new ScavAdapter(sqlCommons, tesauroDataAccess);
            IScavDataAccess scavDataAccess = new ScavDataAccess(sqlCommons, scavAdapter);
            IValidatorFactory validatorFactory = new DefaultValidatorFactory();

            //Dependencias a Inyectar
            IAuthentication auth = new DefaultAuthentication(new SQLCommonsScav(config.ConnectionString));
            IDataAccessService dataService = new DataAccessService(scavDataAccess, validatorFactory);
            IFormularioHtmlService htmlService = new FormularioHtmlService();

            //Tokens
            Cifrador cifrador = new Cifrador(new AES());
            TokenConfig tokenConfig = new TokenConfig();
            ITokenStorage<string> tokenStorage = new TokenStorageDefault(config.DuracionToken);

            ITokenService tokenService = new TokenService(new TokenDefault(cifrador, tokenConfig), tokenStorage);

            //Catálogos
            ICatalogoDataAccess catalogoDataAccess = new CatalogoDataAccess(sqlCommons);

            //Adjuntos
            IAdjuntoDataAccess adjuntoDataAccess = new AdjuntoDataAccess(sqlCommons);
            IAdjuntoService adjuntoService = new AdjuntoService(catalogoDataAccess, adjuntoDataAccess);

            //Logs
            ILogger logger = new Logger(new FileBackend(config.LogsFolder, true));

            _unityContainer.RegisterInstance<IDataAccessService>(dataService);
            _unityContainer.RegisterInstance<IFormularioHtmlService>(htmlService);
            _unityContainer.RegisterInstance<IAuthentication>(auth);
            _unityContainer.RegisterInstance<IConfigQSIAmerica>(config);
            _unityContainer.RegisterInstance<ITokenService>(tokenService);
            _unityContainer.RegisterInstance<ICatalogoDataAccess>(catalogoDataAccess);
            _unityContainer.RegisterInstance<IAdjuntoService>(adjuntoService);
            _unityContainer.RegisterInstance<IFormularioHtmlService>(htmlService);
            _unityContainer.RegisterInstance<ILogger>(logger);
        }
    }
}