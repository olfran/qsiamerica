﻿using QSIAmerica.Models;

namespace QSIAmerica.Lib.Services
{
    /// <summary>
    /// Manejar tokens
    /// </summary>
    public interface ITokenService
    {
        /// <summary>
        /// Generar un token, los datos de entrada pueden ser obtenidos
        /// del modelo LoginResult
        /// </summary>
        /// <param name="userId">ID del usuario</param>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="remoteIP">IP remota</param>
        /// <returns>Token generado o cadena vacía si hubo error</returns>
        string Generate(long userId, string username, string remoteIP);

        /// <summary>
        /// Decodificar tokens
        /// </summary>
        /// <param name="token">Token</param>
        /// <returns>TokenModel</returns>
        TokenModel Decode(string token);

        /// <summary>
        /// Almacenar un token en el almacén de datos
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="token">Token</param>
        /// <returns>Almacenamiento exitoso: true, de lo contrario false</returns>
        bool Save(string key, string token);

        /// <summary>
        /// Obtener un token
        /// </summary>
        /// <param name="key">Token</param>
        /// <returns>Token o string vacío si hubo error</returns>
        string Get(string key);

        /// <summary>
        /// Eliminar un token del almacén de datos
        /// </summary>
        /// <param name="key">Token</param>
        void Delete(string key);

        /// <summary>
        /// Verificar si un Token es válido
        /// </summary>
        /// <param name="token">Token</param>
        /// <param name="ip">IP remota</param>
        /// <returns>True si el Token es válido de lo contrario false</returns>
        bool IsValidToken(string token, string ip);

        /// <summary>
        /// Verificar si un Token es válido
        /// </summary>
        /// <param name="tokenModel">Token</param>
        /// <param name="ip">IP remota</param>
        /// <returns>True si el Token es válido de lo contrario false</returns>
        bool IsValidToken(TokenModel tokenModel, string ip);
    }
}