﻿using QSIAmerica.Lib.Token;
using QSIAmerica.Models;

namespace QSIAmerica.Lib.Services
{
    public class TokenService : ITokenService
    {
        readonly ITokenStorage<string> _storage;
        readonly IToken _token;

        public TokenService(IToken token, ITokenStorage<string> storage)
        {
            _token = token;
            _storage = storage;
        }

        /// <summary>
        /// Generar un token, los datos de entrada pueden ser obtenidos
        /// del modelo LoginResult
        /// </summary>
        /// <param name="userId">ID del usuario</param>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="remoteIP">IP remota</param>
        /// <returns>Token generado o cadena vacía si hubo error</returns>
        public string Generate(long userId, string username, string remoteIP)
        {
            return _token.Generate(userId, username, remoteIP);
        }

        /// <summary>
        /// Decodificar tokens
        /// </summary>
        /// <param name="token">Token</param>
        /// <returns>TokenModel</returns>
        public TokenModel Decode(string token)
        {
            return _token.Decode(token);
        }

        /// <summary>
        /// Almacenar un token en el almacén de datos
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>Almacenamiento exitoso: true, de lo contrario false</returns>
        public bool Save(string key, string token)
        {
            return _storage.Save(key, token);
        }

        /// <summary>
        /// Obtener un token
        /// </summary>
        /// <param name="key">Token</param>
        /// <returns>Token o string vacío si hubo error</returns>
        public string Get(string key)
        {
            return _storage.Get(key);
        }

        /// <summary>
        /// Eliminar un token del almacén de datos
        /// </summary>
        /// <param name="key">Token</param>
        public void Delete(string key)
        {
            _storage.Delete(key);
        }

        /// <summary>
        /// Verificar si un Token es válido
        /// </summary>
        /// <param name="tokenModel">Token</param>
        /// <param name="ip">IP remota</param>
        /// <returns>True si el Token es válido de lo contrario false</returns>
        public bool IsValidToken(TokenModel tokenModel, string ip)
        {
            string currentTokenString = Get(tokenModel.Username);

            if (!string.IsNullOrEmpty(currentTokenString))
            {
                //Verificar si las IPs (Request y Token) son las mismas, para prevenir que otro usuario utilice el token
                //generado originalmente

                return tokenModel.RemoteIP == ip;
            }

            return false;
        }

        /// <summary>
        /// Verificar si un Token es válido
        /// </summary>
        /// <param name="token">Token</param>
        /// <param name="ip">IP remota</param>
        /// <returns>True si el Token es válido de lo contrario false</returns>
        public bool IsValidToken(string token, string ip)
        {
            var currentToken = Decode(token);
            string currentTokenString = Get(currentToken.Username);

            if (!string.IsNullOrEmpty(currentTokenString))
            {
                //Verificar si las IPs (Request y Token) son las mismas, para prevenir que otro usuario utilice el token
                //generado originalmente

                return currentToken.RemoteIP == ip;
            }

            return false;
        }
    }
}