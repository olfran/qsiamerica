﻿using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.DataAccess.Adapters;
using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;
using System.IO;

namespace QSIAmerica.Lib.Services
{
    public class AdjuntoService : IAdjuntoService
    {
        readonly IAdjuntoDataAccess _adjuntoDataAccess;
        readonly ICatalogoDataAccess _catalogoDataAccess;

        public AdjuntoService(ICatalogoDataAccess catalogoDataAccess, IAdjuntoDataAccess adjuntoDataAccess)
        {
            _catalogoDataAccess = catalogoDataAccess;
            _adjuntoDataAccess = adjuntoDataAccess;
        }

        /// <summary>
        /// Retorna todos los adjuntos pertenecientes al documento
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <returns>Lista o lista vacía si no hay ningún adjunto</returns>
        public IList<AdjuntoPropiedades> GetAdjuntos(long idCatalogo, long idDocumento)
        {
            return _adjuntoDataAccess.GetAdjuntos(idCatalogo, idDocumento);
        }

        /// <summary>
        /// Encuentra un adjunto
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <param name="idAdjunto">Id adjunto</param>
        /// <returns>Datos del adjunto</returns>
        public AdjuntoPropiedades FindAdjunto(long idCatalogo, long idDocumento, long idAdjunto)
        {
            return _adjuntoDataAccess.FindAdjunto(idCatalogo, idDocumento, idAdjunto);
        }

        /// <summary>
        /// Retorna el path físico completo correspondiente al adjunto
        /// </summary>
        /// <returns>Path o string vacío si hubo error</returns>
        public string GetPathAdjunto(long idCatalogo, long idDocumento, long idAdjunto)
        {
            var adjuntoPropiedades = _adjuntoDataAccess.FindAdjunto(idCatalogo, idDocumento, idAdjunto);

            return ResolvePath(idCatalogo, idDocumento, adjuntoPropiedades.NombreFisico);
        }

        /// <summary>
        /// Retorna el path físico completo correspondiente al adjunto
        /// </summary>
        /// <returns>Path o string vacío si hubo error</returns>
        public string GetPathAdjunto(long idCatalogo, long idDocumento, AdjuntoPropiedades adjuntoPropiedades)
        {
            return ResolvePath(idCatalogo, idDocumento, adjuntoPropiedades.NombreFisico);

        }

        string ResolvePath(long idCatalogo, long idDocumento, string nombre)
        {
            string resultado = "";
            var directorios = _catalogoDataAccess.GetDirectorios(idCatalogo);

            IList<string> paths = new List<string>
            {
                Path.Combine(GetDirectoriosPorId(idDocumento, directorios.Archivo), nombre),
                Path.Combine(GetDirectoriosPorId(idDocumento, directorios.Multimedia), nombre),
                Path.Combine(GetDirectoriosPorId(idDocumento, directorios.Imagen), nombre),
                Path.Combine(GetDirectoriosPorId(idDocumento, directorios.Texto), nombre + ".txs"),
                Path.Combine(GetDirectoriosPorId(idDocumento, directorios.Web), nombre)
            };

            foreach (var path in paths)
            {
                if (File.Exists(path))
                {
                    resultado = path;
                    break;
                }
            }

            return resultado;
        }

        /// <summary>
        /// Método sacado del código fuente de AIRE/SCAV
        /// </summary>
        string GetDirectoriosPorId(long idDocumento, string rutaBase)
        {
            if (rutaBase.Substring(rutaBase.Length - 1, 1) != "\\") { rutaBase = rutaBase + "\\"; }
            string stringId3Digitos = idDocumento.ToString().PadLeft(3, '0');
            if (stringId3Digitos.Length > 3) { stringId3Digitos = stringId3Digitos.Substring(stringId3Digitos.Length - 3); }
            if (!Directory.Exists(rutaBase + "D" + stringId3Digitos.Substring(0, 1))) { Directory.CreateDirectory(rutaBase + "D" + stringId3Digitos.Substring(0, 1)); }
            if (!Directory.Exists(rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1))) { Directory.CreateDirectory(rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1)); }
            if (!Directory.Exists(rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1) + "\\D" + stringId3Digitos.Substring(2, 1))) { Directory.CreateDirectory(rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1) + "\\D" + stringId3Digitos.Substring(2, 1)); }
            return rutaBase + "D" + stringId3Digitos.Substring(0, 1) + "\\D" + stringId3Digitos.Substring(1, 1) + "\\D" + stringId3Digitos.Substring(2, 1) + "\\";
        }
    }
}