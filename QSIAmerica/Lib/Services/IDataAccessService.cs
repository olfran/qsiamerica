﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;

namespace QSIAmerica.Lib.Services
{
    /// <summary>
    /// Servicio para el acceso a datos de los Catálogos de Scav, basicamente sirve de Wrapper
    /// para IScavDataAccess, por los momentos no es testeable ya que tiene muchas dependencias
    /// por lo que actúa de contenedor.
    /// Nota: Siempre se debe llamar a Initialize antes de utilizar los métodos de la clase
    /// </summary>
    public interface IDataAccessService
    {
        /// <summary>
        /// Retorna los campos del formulario
        /// </summary>
        IList<Campo> GetCamposFormulario(long idCatalogo, int tipoFormulario, bool cargarTesauros = false);

        /// <summary>
        /// Validar todos los campos de la lista, los mensajes quedan almacenados
        /// en la lista de Mensajes
        /// </summary>
        /// <param name="campos"></param>
        void ValidarCampos(IList<Campo> campos);

        /// <summary>
        /// Crear un Documento en Scav
        /// </summary>
        /// <returns>ID_DOCUMENTO si todo fue bien de lo contrario 0</returns>
        long CrearDocumento(IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Actualizar un Documento en Scav
        /// </summary>
        /// <returns>true si todo fue bien, de lo contrario false</returns>
        bool ActualizarDocumento(IList<Campo> campos, long idCatalogo, long idDocumento);

        /// <summary>
        /// Reliza una búsqueda, con el valor de cada campo por separado
        /// retorna una lista de documentos o una lista vacía si no hubo resultados
        /// </summary>
        ///<param name="campos">Campos a buscar con sus respectivos valores</param>
        ///<param name="idCatalogo">ID del Catálogo al cual pertenecen los campos</param>
        IList<Documento> Buscar(IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Reliza una búsqueda amplia, sin filtro, en todos los campos del catálogo deseado y 
        /// retorna una lista de documentos o una lista vacía si no hubo resultados
        /// </summary>
        ///<param name="query">Texto a buscar en cada campo</param>
        ///<param name="campos">Campos a buscar con sus respectivos valores</param>
        ///<param name="idCatalogo">ID del Catálogo al cual pertenecen los campos</param>
        IList<Documento> BusquedaAmplia(string query, IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Inicializar las dependencias
        /// </summary>
        void SetPaginador(Paginador paginador = null);

        /// <summary>
        /// Retorna el total de registros de la búsqueda amplia de un catálogo, de manera eficiente omitiendo la paginación
        /// </summary>
        long GetTotalBusquedaAmplia(string q, IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Llenar el Documento con todos los datos
        /// </summary>
        /// <returns>Lista de todos los Documentos con sus respectivos valores o una lista vacía si hubo error</returns>
        Documento GetDocumento(IList<Campo> campos, long idCatalogo, long idDocumento);

        /// <summary>
        /// Trata de encontrar todos los documentos del Catálogo junto con los campos y sus respectivos
        /// valores, si los campos tienen algún valor se crea una consulta WHERE and con sus valores.
        /// Los resultados se regresan paginados dependiendo de la configuración del paginador en la dependencia
        /// ISQLCommonsScav
        /// </summary>
        /// <returns>Lista de Documentos con sus respectivos valores o una lista vacía si hubo error</returns>
        IList<Documento> FindDocumentos(IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Permite validar los Campos Únicos del Documento
        /// </summary>
        /// <param name="campos">Campos</param>
        /// <param name="idDocumentoEditando">Opcional: ID del documento si se está editando</param>
        void ValidarCamposUnicos(IList<Campo> campos, long idDocumentoEditando = 0);

        /// <summary>
        /// Retorna el total de registros de un Catálogo de manera eficiente
        /// </summary>
        long GetTotal(long idCatalogo, IList<Campo> camposFiltro = null);

        /// <summary>
        /// Retorna el total de registros de una búsqueda de manera eficiente
        /// </summary>
        long GetTotalBuscar(long idCatalogo, IList<Campo> camposFiltro);

        /// <summary>
        /// Permite enviar un Flujo de Trabajo
        /// </summary>
        bool EnviarFlujo(long idCatalogo, long idDocumento);

        /// <summary>
        /// Operador lógico para el método Buscar
        /// 0 == AND 1 == OR, por defecto AND
        /// </summary>
        int OperadorLogicoBuscar { get; set; }
    }
}
