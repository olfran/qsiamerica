﻿using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.Models;
using ScavUtils.Validators;
using System;
using System.Collections.Generic;

namespace QSIAmerica.Lib.Services
{
    /// <summary>
    /// Servicio para el acceso a datos de los Catálogos de Scav, basicamente sirve de Wrapper
    /// para IScavDataAccess, por los momentos no es testeable ya que tiene muchas dependencias
    /// por lo que actúa de contenedor.
    /// Nota: Siempre se debe llamar a Initialize antes de utilizar los métodos de la clase
    /// </summary>
    public class DataAccessService : IDataAccessService
    {
        IScavDataAccess _scavDataAccess;
        IValidatorFactory _validatorFactory;

        public int OperadorLogicoBuscar
        {
            get
            {
                return _scavDataAccess.OperadorLogicoBuscar;
            }

            set
            {
                _scavDataAccess.OperadorLogicoBuscar = value;
            }
        }

        public DataAccessService(IScavDataAccess scavDataAccess, IValidatorFactory validatorFactory)
        {
            _scavDataAccess = scavDataAccess;
            _validatorFactory = validatorFactory;
        }

        /// <summary>
        /// Establecer el Paginador
        /// </summary>
        public void SetPaginador(Paginador paginador)
        {
            _scavDataAccess.SetPaginador(paginador);
        }

        public IList<Campo> GetCamposFormulario(long idCatalogo, int tipoFormulario, bool cargarTesauros = false)
        {
            return _scavDataAccess.GetCamposFormulario(idCatalogo, tipoFormulario, cargarTesauros);
        }

        public void ValidarCampos(IList<Campo> campos)
        {
            SetValidators(campos);

            foreach (Campo campo in campos)
            {
                campo.Validate();
            }
        }

        /// <summary>
        /// Establecer los distintos validadores
        /// </summary>
        public void SetValidators(IList<Campo> campos)
        {
            _validatorFactory.SetValidators(campos);
        }

        /// <summary>
        /// Reliza una búsqueda, con el valor de cada campo por separado
        /// retorna una lista de documentos o una lista vacía si no hubo resultados
        /// </summary>
        ///<param name="campos">Campos a buscar con sus respectivos valores</param>
        ///<param name="idCatalogo">ID del Catálogo al cual pertenecen los campos</param>
        public IList<Documento> Buscar(IList<Campo> campos, long idCatalogo)
        {
            return _scavDataAccess.Buscar(campos, idCatalogo);
        }

        /// <summary>
        /// Reliza una búsqueda amplia, sin filtro, en todos los campos del catálogo deseado y 
        /// retorna una lista de documentos o una lista vacía si no hubo resultados
        /// </summary>
        ///<param name="query">Texto a buscar en cada campo</param>
        ///<param name="campos">Campos a buscar con sus respectivos valores</param>
        ///<param name="idCatalogo">ID del Catálogo al cual pertenecen los campos</param>
        public IList<Documento> BusquedaAmplia(string query, IList<Campo> campos, long idCatalogo)
        {
            return _scavDataAccess.BusquedaAmplia(query, campos, idCatalogo);
        }

        /// <summary>
        /// Crear un Documento en SCAV
        /// </summary>
        /// <returns>ID_DOCUMENTO si todo fue bien de lo contrario 0</returns>
        public long CrearDocumento(IList<Campo> campos, long idCatalogo)
        {
            return _scavDataAccess.CrearDocumento(campos, idCatalogo);
        }

        /// <summary>
        /// Actualizar un Documento en SCAV
        /// </summary>
        /// <returns>true si todo fue bien, de lo contrario false</returns>
        public bool ActualizarDocumento(IList<Campo> campos, long idCatalogo, long idDocumento)
        {
            return _scavDataAccess.ActualizarDocumento(campos, idCatalogo, idDocumento);
        }

        //Retorna el total de registros
        public long GetTotalBusquedaAmplia(string q, IList<Campo> campos, long idCatalogo)
        {
            return _scavDataAccess.GetTotalBusquedaAmplia(q, campos, idCatalogo);
        }

        /// <summary>
        /// Llenar el Documento con todos los datos
        /// </summary>
        /// <returns>Lista de todos los Documentos con sus respectivos valores o una lista vacía si hubo error</returns>
        public Documento GetDocumento(IList<Campo> campos, long idCatalogo, long idDocumento)
        {
            return _scavDataAccess.GetDocumento(campos, idCatalogo, idDocumento);
        }

        /// <summary>
        /// Trata de encontrar todos los documentos del Catálogo junto con los campos y sus respectivos
        /// valores, si los campos tienen algún valor se crea una consulta WHERE and con sus valores.
        /// Los resultados se regresan paginados dependiendo de la configuración del paginador en la dependencia
        /// ISQLCommonsScav
        /// </summary>
        /// <returns>Lista de Documentos con sus respectivos valores o una lista vacía si hubo error</returns>
        public IList<Documento> FindDocumentos(IList<Campo> campos, long idCatalogo)
        {
            return _scavDataAccess.FindDocumentos(campos, idCatalogo);
        }


        /// <summary>
        /// Permite validar los Campos Únicos del Documento
        /// </summary>
        /// <param name="campos">Campos</param>
        /// <param name="idDocumentoEditando">Opcional: ID del documento si se está editando</param>
        public void ValidarCamposUnicos(IList<Campo> campos, long idDocumentoEditando = 0)
        {
            foreach (Campo campo in campos)
            {
                if (campo.Unico)
                {
                    IList<Campo> campoABuscar = new List<Campo>()
                    { 
                        campo
                    };

                    IList<Documento> documentos = FindDocumentos(campoABuscar, campo.IdCatalogo);

                    //Si no encuentra ningún documento, quiere decir que no se repite
                    if (documentos.Count > 0 && documentos[0].Id != idDocumentoEditando)
                    {
                        campo.IsValid = false;
                        campo.Mensajes.Add(String.Format("El campo '{0}' debe ser único, ese valor ya se encuentra registrado en nuestra Base de Datos", campo.Titulo));
                    }
                }
            }
        }

        /// <summary>
        /// Retorna el total de registros de un Catálogo de manera eficiente
        /// </summary>
        public long GetTotal(long idCatalogo, IList<Campo> camposFiltro = null)
        {
            return _scavDataAccess.GetTotal(idCatalogo, camposFiltro);
        }

        /// <summary>
        /// Retorna el total de registros de una búsqueda de manera eficiente
        /// </summary>
        public long GetTotalBuscar(long idCatalogo, IList<Campo> camposFiltro)
        {
            return _scavDataAccess.GetTotalBuscar(idCatalogo, camposFiltro);
        }

        /// <summary>
        /// Permite enviar un Flujo de Trabajo
        /// </summary>
        public bool EnviarFlujo(long idCatalogo, long idDocumento)
        {
            return _scavDataAccess.EnviarFlujo(idCatalogo, idDocumento);
        }
    }
}