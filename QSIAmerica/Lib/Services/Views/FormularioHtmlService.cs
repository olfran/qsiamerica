﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace QSIAmerica.Lib.Services.Views
{
    public class FormularioHtmlService : IFormularioHtmlService
    {
        /// <summary>
        /// Hace un bind de los datos de una colección al modelo de campos
        /// ideal para ser usado con Request.Query o FormCollection por ejemplo
        /// </summary>
        public void BindRequest(IList<Campo> campos, NameValueCollection parametros)
        {
            foreach (Campo campo in campos)
            {
                if (parametros[campo.Nombre] != null)
                {
                    //Siempre hacer encode para evitar ataques tipo XSS
                    //Razor siempre codifica la salida a HTML salvo se especifique explicítamente
                    //que no lo haga con @Html.Raw() (No conviene hacer esto)
                    campo.Valor = parametros[campo.Nombre];
                }
            }
        }
    }
}