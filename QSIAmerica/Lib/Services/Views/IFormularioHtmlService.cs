﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace QSIAmerica.Lib.Services.Views
{
    public interface IFormularioHtmlService
    {
        void BindRequest(IList<Campo> campos, NameValueCollection parametros);
    }
}
