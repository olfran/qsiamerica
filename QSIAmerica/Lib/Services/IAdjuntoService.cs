﻿using System.Collections.Generic;
using ScavUtils.Infrastructure.Models;

namespace QSIAmerica.Lib.Services
{
    public interface IAdjuntoService
    {
        /// <summary>
        /// Retorna todos los adjuntos pertenecientes al documento
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <returns>Lista o lista vacía si no hay ningún adjunto</returns>
        IList<AdjuntoPropiedades> GetAdjuntos(long idCatalogo, long idDocumento);

        /// <summary>
        /// Encuentra un adjunto
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <param name="idAdjunto">Id adjunto</param>
        /// <returns>Datos del adjunto</returns>
        AdjuntoPropiedades FindAdjunto(long idCatalogo, long idDocumento, long idAdjunto);

        /// <summary>
        /// Retorna el path físico completo correspondiente al adjunto
        /// </summary>
        /// <returns>Path o string vacío si hubo error</returns>
        string GetPathAdjunto(long idCatalogo, long idDocumento, long idAdjunto);

        /// <summary>
        /// Retorna el path físico completo correspondiente al adjunto
        /// </summary>
        /// <returns>Path o string vacío si hubo error</returns>
        string GetPathAdjunto(long idCatalogo, long idDocumento, AdjuntoPropiedades adjuntoPropiedades);
    }
}