﻿namespace QSIAmerica.Lib.Authentication
{
    public interface IAuthentication
    {
        /// <summary>
        /// Tratar de hacer login en el sistema AIRE/SCAV
        /// </summary>
        AuthenticationResult Login(string username, string password);
        
        /// <summary>
        /// Logout
        /// </summary>
        void Logout();
    }
}