﻿using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Security;

namespace QSIAmerica.Lib.Authentication
{
    public class DefaultAuthentication : IAuthentication
    {
        ISQLCommonsScav _sqlCommons;

        public DefaultAuthentication(ISQLCommonsScav sqlCommons)
        {
            _sqlCommons = sqlCommons;
        }

        /// <summary>
        /// Tratar de hacer login contra la base de datos de usuario
        /// </summary>
        /// <returns>
        /// AuthenticationResult si la autenticación fue exitosa, de lo contrario null
        /// </returns>
        public AuthenticationResult Login(string username, string password)
        {
            string select = "SELECT USUARIO.ID_USUARIO,USUARIO.NOMBRE_USUARIO,USUARIO.CLAVE,ID_CATALOGO FROM USUARIO WHERE (USUARIO.LOGIN=? AND ESTATUS_USER=1)";
            Clave clave = new Clave();
            Dictionary<string, object> parametros = new Dictionary<string, object>()
            {
                { "@p1", username }
            };

            AuthenticationResult resultado = new AuthenticationResult() { Error = true };

            _sqlCommons.ExecuteQuery(select, parametros, (error, reader) =>
            {
                if (!error)
                {
                    try
                    {
                        reader.Read();
                        if (reader.FieldCount > 0)
                        {
                            resultado.IdUsuario = _sqlCommons.ParseLong(reader[0].ToString());
                            resultado.NombreUsuario = reader[1].ToString();
                            resultado.Error = (password != clave.desencriptaClave(reader[2].ToString())) || (resultado.IdUsuario == 0);
                            resultado.IdCatalogo = _sqlCommons.ParseLong(reader[3].ToString());

                            if (resultado.Error)
                            {
                                resultado.Message = "Nombre de usuario o contraseña inválida";
                            }
                        }
                    }
                    catch
                    {
                        resultado.Error = true;
                    }
                } 
                else
                {
                    resultado.Message = "Error conectando a la base de datos";
                }
            });

            return resultado;
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }

        /// <summary>
        /// Clase extraida de AIRE/SCAV
        /// </summary>
        private class Clave
        {
            public string desencriptaClave(string clave)
            {
                int largo;
                string valor;
                try
                {
                    valor = clave.Substring(82, 1) + clave.Substring(4, 1);
                    largo = int.Parse(valor);
                    //7-9-14-15-23-20-33-40-41-49-48-52-56-61-69-72-73-84-98-99
                    clave = clave.Substring(6, 1) + clave.Substring(8, 1) + clave.Substring(13, 1) + clave.Substring(14, 1) + clave.Substring(22, 1) + clave.Substring(19, 1) + clave.Substring(32, 1) + clave.Substring(39, 1) + clave.Substring(40, 1) + clave.Substring(48, 1) + clave.Substring(98, 1) + clave.Substring(51, 1) + clave.Substring(55, 1) + clave.Substring(60, 1) + clave.Substring(68, 1) + clave.Substring(71, 1) + clave.Substring(72, 1) + clave.Substring(83, 1) + clave.Substring(97, 1) + clave.Substring(47, 1);
                    return murcielago(clave, largo);

                }
                catch
                {
                    return "";
                }
            }
            public string encriptaClave(string clave)
            {
                string p = ""; string palabra = "";
                string largo = rellenaCeros(clave.Length.ToString(), 2);
                Random getrandom = new Random();
                for (int i = 1; i < 100; i++)
                {
                    int a = 0;
                    while (!(((a > 48) && (a < 57)) || ((a > 65) && (a < 122)) && a != 92))
                    {
                        a = getrandom.Next(48, 122);
                    }
                    string letra = Char.ConvertFromUtf32(a);
                    //7-9-14-15-23-20-33-40-41-49-48-52-56-61-69-72-73-84-98-99
                    p = "";
                    switch (i)
                    {
                        case 7:
                            try { p = clave.Substring(0, 1); } catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 9:
                            try { p = clave.Substring(1, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 14:
                            try { p = clave.Substring(2, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 15:
                            try { p = clave.Substring(3, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 23:
                            try { p = clave.Substring(4, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 20:
                            try { p = clave.Substring(5, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 33:
                            try { p = clave.Substring(6, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 40:
                            try { p = clave.Substring(7, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 41:
                            try { p = clave.Substring(8, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 49:
                            try { p = clave.Substring(9, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 99:
                            try { p = clave.Substring(10, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 52:
                            try { p = clave.Substring(11, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 56:
                            try { p = clave.Substring(12, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 61:
                            try { p = clave.Substring(13, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 69:
                            try { p = clave.Substring(14, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 72:
                            try { p = clave.Substring(15, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 73:
                            try { p = clave.Substring(16, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 84:
                            try { p = clave.Substring(17, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 98:
                            try { p = clave.Substring(18, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                        case 48:
                            try { p = clave.Substring(19, 1); }
                            catch { }
                            if (p != "") { letra = murci(p); }
                            break;
                    }
                    palabra = palabra + letra;
                }

                StringBuilder pepe = new StringBuilder();

                pepe.Append(palabra.ToString());
                pepe.Remove(4, 1);
                pepe.Insert(4, largo.Substring(largo.Length - 1, 1));

                pepe.Remove(82, 1);
                pepe.Insert(82, largo.Substring(0, 1));
                return pepe.ToString();
            }
            private string murcielago(string clave, int largo)
            {
                string varia; string murci = "";
                for (int i = 1; i <= largo; i++)
                {
                    varia = clave.Substring(i - 1, 1);
                    switch (varia)
                    {
                        case "M":
                            murci = murci + "1";
                            break;
                        case "U":
                            murci = murci + "2";
                            break;
                        case "R":
                            murci = murci + "3";
                            break;
                        case "C":
                            murci = murci + "4";
                            break;
                        case "I":
                            murci = murci + "5";
                            break;
                        case "E":
                            murci = murci + "6";
                            break;
                        case "L":
                            murci = murci + "7";
                            break;
                        case "A":
                            murci = murci + "8";
                            break;
                        case "G":
                            murci = murci + "9";
                            break;
                        case "O":
                            murci = murci + "0";
                            break;
                        case "1":
                            murci = murci + "M";
                            break;
                        case "2":
                            murci = murci + "U";
                            break;
                        case "3":
                            murci = murci + "R";
                            break;
                        case "4":
                            murci = murci + "C";
                            break;
                        case "5":
                            murci = murci + "I";
                            break;
                        case "6":
                            murci = murci + "E";
                            break;
                        case "7":
                            murci = murci + "L";
                            break;
                        case "8":
                            murci = murci + "A";
                            break;
                        case "9":
                            murci = murci + "G";
                            break;
                        case "0":
                            murci = murci + "O";
                            break;
                        default:
                            murci = murci + varia;
                            break;
                    }
                }
                return murci;
            }
            private string murci(string letra)
            {
                string murciAux = "";
                switch (letra)
                {
                    case "M":
                        murciAux = "1";
                        break;
                    case "U":
                        murciAux = "2";
                        break;
                    case "R":
                        murciAux = "3";
                        break;
                    case "C":
                        murciAux = "4";
                        break;
                    case "I":
                        murciAux = "5";
                        break;
                    case "E":
                        murciAux = "6";
                        break;
                    case "L":
                        murciAux = "7";
                        break;
                    case "A":
                        murciAux = "8";
                        break;
                    case "G":
                        murciAux = "9";
                        break;
                    case "O":
                        murciAux = "0";
                        break;
                    case "1":
                        murciAux = "M";
                        break;
                    case "2":
                        murciAux = "U";
                        break;
                    case "3":
                        murciAux = "R";
                        break;
                    case "4":
                        murciAux = "C";
                        break;
                    case "5":
                        murciAux = "I";
                        break;
                    case "6":
                        murciAux = "E";
                        break;
                    case "7":
                        murciAux = "L";
                        break;
                    case "8":
                        murciAux = "A";
                        break;
                    case "9":
                        murciAux = "G";
                        break;
                    case "0":
                        murciAux = "O";
                        break;
                    default:
                        murciAux = letra;
                        break;
                }
                return murciAux;
            }

            /// <summary>
            /// Agrega la cantidad de ceros faltantes a un valor segun el formato (cantidad de ceros)
            /// </summary>
            /// <param name="valor"></param>
            /// <param name="cantidadDeCeros"></param>
            /// <param name="parteDerecha">Para valores decimales agrega ceros a la derecha, false por defecto</param>
            /// <returns></returns>
            private string rellenaCeros(string valor, int cantidadDeCeros, bool parteDerecha = false)
            {
                string nuevoValor = valor;
                if (valor.Length < cantidadDeCeros)
                {
                    for (int i = valor.Length; i < cantidadDeCeros; i++)
                    {
                        if (parteDerecha) { nuevoValor = nuevoValor + "0"; }
                        else
                        {
                            nuevoValor = "0" + nuevoValor;
                        }
                    }
                }
                return nuevoValor;
            }
        }
    }
}