﻿namespace QSIAmerica.Lib.Authentication
{
    /// <summary>
    /// Modelo para el resultado de una autenticación exitosa
    /// </summary>
    public class AuthenticationResult
    {
        public long IdUsuario { get; set; }
        public long IdCatalogo { get; set; }
        public string NombreUsuario { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
    }
}