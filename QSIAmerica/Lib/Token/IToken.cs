﻿using QSIAmerica.Models;

namespace QSIAmerica.Lib.Token
{
    /// <summary>
    /// Generación de tokens, dentro del token están los campos de la clase login result
    /// encriptados con la clave TokenConfig.Secret y separados por TokenConfig.Separator
    /// </summary>
    public interface IToken
    {
        /// <summary>
        /// Generar un token
        /// </summary>
        /// <param name="userId">ID del usuario</param>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="remoteIP">IP remota</param>
        /// <returns>Token generado o cadena vacía si hubo error</returns>
        string Generate(long userId, string username, string remoteIP);

        /// <summary>
        /// Decodificar un token
        /// </summary>
        /// <param name="token">token a decodificar</param>
        /// <returns>Modelo</returns>
        TokenModel Decode(string token);
    }

    /// <summary>
    /// Configuración del token, establece datos por defecto
    /// </summary>
    public class TokenConfig
    {
        public string Secret { get; set; }
        public string Separator { get; set; }

        public TokenConfig()
        {
            Secret = "3!wl^,-J'\\Bvqc^JPH]j648?WAx74\"28c3P1/98rq$c?=+o$YN\\^!tjn:}5p";
            Separator = "_|!#_";
        }
    }
}