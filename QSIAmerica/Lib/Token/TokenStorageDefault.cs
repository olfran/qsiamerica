﻿using System;
using System.Web;
using System.Web.Caching;

namespace QSIAmerica.Lib.Token
{
    /// <summary>
    /// Implementa el almacenamiento de tokens en el Cache de ASP.NET
    /// </summary>
    public class TokenStorageDefault : ITokenStorage<string>
    {
        readonly int _duracionCache;
        static readonly object _locker = new object();

        /// <summary>
        /// Constructor que acepta como parámetro la duración del Token en memoria.
        /// Por defecto la duración en cache es de 30 minutos
        /// </summary>
        /// <param name="duracionCache"></param>
        public TokenStorageDefault(int duracionCache = 30)
        {
            _duracionCache = duracionCache;
        }

        public void Delete(string token)
        {
            HttpContext.Current.Cache.Remove(token);
        }

        public string Get(string token)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Cache.Get(token) != null ? HttpContext.Current.Cache.Get(token).ToString() : "";
            }

            return "";
        }

        public bool Save(string key, string token)
        {
            //Verificar concurrencia
            lock (_locker)
            {
                try
                {
                    HttpContext.Current.Cache
                        .Add(
                        key,
                        token,
                        null,
                        DateTime.Now.AddMinutes((double)_duracionCache),
                        Cache.NoSlidingExpiration,
                        CacheItemPriority.Normal,
                        null);
                }
                catch
                {
                    return false;
                }

            }

            return true;
        }
    }
}