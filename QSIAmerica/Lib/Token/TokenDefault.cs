﻿using System;
using QSIAmerica.Models;
using Archicentro.Criptografia.Cifrado;
using System.Text;

namespace QSIAmerica.Lib.Token
{
    /// <summary>
    /// Generador de Tokens por defecto
    /// </summary>
    public class TokenDefault : IToken
    {
        readonly ICifrador _cifrador;
        readonly TokenConfig _config;

        public TokenDefault(ICifrador cifrador, TokenConfig config)
        {
            _cifrador = cifrador;
            _config = config;
        }

        /// <summary>
        /// Generar un token
        /// </summary>
        /// <param name="userId">ID del usuario</param>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="remoteIP">IP remota</param>
        /// <returns>Token generado o cadena vacía si hubo error</returns>
        public string Generate(long userId, string username, string remoteIP)
        {
            return EncodeHelper(_cifrador.Cifrar(GetBytes(JoinPieces(userId, username, remoteIP)), _config.Secret));
        }

        public TokenModel Decode(string token)
        {
            var decoded = DecodeHelper(token);
            var decrypted = _cifrador.Descifrar(decoded, _config.Secret);
            var loginResultString = GetString(decrypted);

            return MapToLoginResult(loginResultString);
        }

        TokenModel MapToLoginResult(string loginResultString)
        {
            var splitted = loginResultString.Split(new string[] { _config.Separator }, StringSplitOptions.None);
            long userId = 0, timestamp = 0;

            long.TryParse(splitted[0], out userId);
            long.TryParse(splitted[3], out timestamp);

            return new TokenModel
            {
                UserId = userId,
                Username = splitted[1],
                RemoteIP = splitted[2],
                GenerationTime = DateTime.FromFileTime(timestamp)
            };
        }

        string EncodeHelper(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }

        byte[] DecodeHelper(string str)
        {
            return Convert.FromBase64String(str);
        }

        byte[] GetBytes(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        string GetString(byte[] bytes)
        {
            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        /// Retorna los datos unidos por el separador
        /// UserId, Username, IP, GenerationTime
        /// </summary>
        /// <param name="userId">ID del usuario</param>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="remoteIP">IP remota</param>
        /// <returns>Piezas unidas por el separador</returns>
        string JoinPieces(long userId, string username, string remoteIP)
        {
            return String.Format("{1}{0}{2}{0}{3}{0}{4}", _config.Separator, userId, username, remoteIP, DateTime.Now.ToFileTime());
        }
    }
} 