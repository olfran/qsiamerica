﻿namespace QSIAmerica.Lib.Token
{
    public interface ITokenStorage<T>
    {
        /// <summary>
        /// Permite guardar información en el orígen de datos usando como clave el token
        /// </summary>
        /// <param name="key">Key del diccionario</param>
        /// <param name="token">Token a almacenar, puede ser obtenido con el método GenerarToken</param>
        /// <returns>true si tuvo éxito de lo contratio false</returns>
        bool Save(string key, string token);

        /// <summary>
        /// Permite eliminar el Token del orígen de datos
        /// </summary>
        /// <param name="token">Token a eliminar</param>
        /// <returns>void</returns>
        void Delete(string token);

        /// <summary>
        /// Retorna el contenido almacenado en orígen de datos usando como clave el Token
        /// </summary>
        /// <param name="token">Token</param>
        /// <returns>Datos almacenados, si hubo error retorna default(T)</returns>
        T Get(string token);
    }
}
