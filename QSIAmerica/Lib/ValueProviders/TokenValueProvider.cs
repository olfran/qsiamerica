﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;

namespace QSIAmerica.Lib.ValueProviders
{
    /// <summary>
    /// Permite obtener el token del Header, se puede utilizar en los actions
    /// siempre y cuando el parámetro del método se llame token
    /// </summary>
    public class TokenValueProvider : System.Web.Mvc.IValueProvider
    {
        readonly NameValueCollection _headers;

        const string TokenHeader = "X-Archicentro-Token";
        const string ParamName = "token";
        HttpServerUtilityBase _server;

        public TokenValueProvider(NameValueCollection headers, HttpServerUtilityBase server)
        {
            this._headers = headers;
            this._server = server;
        }

        public bool ContainsPrefix(string prefix)
        {
            return prefix == ParamName;
        }

        public System.Web.Mvc.ValueProviderResult GetValue(string key)
        {
            if (key == ParamName)
            {
                string val = _server.UrlDecode(_headers.Get(TokenHeader));
                return new System.Web.Mvc.ValueProviderResult(val, val, CultureInfo.InvariantCulture);
            }

            return null;
        }
    }

    public class TokenValueProviderFactory : ValueProviderFactory
    {
        public override System.Web.Mvc.IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            var headers = controllerContext.HttpContext.Request.Headers;
            return new TokenValueProvider(headers, controllerContext.HttpContext.Server);
        }
    }
}