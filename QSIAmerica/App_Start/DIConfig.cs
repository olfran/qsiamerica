﻿using System.Web.Mvc;

namespace QSIAmerica
{
    public static class DIConfig
    {
        public static void RegisterDependencyResolver(IDependencyResolver resolver)
        {
            DependencyResolver.SetResolver(resolver);
        }
    }
}
