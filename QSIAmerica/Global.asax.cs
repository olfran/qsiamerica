﻿using Logging;
using QSIAmerica.Lib.DI;
using QSIAmerica.Lib.ValueProviders;
using System;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;

namespace QSIAmerica
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            DIConfig.RegisterDependencyResolver(new UnityResolver());
            ValueProviderFactories.Factories.Insert(0, new TokenValueProviderFactory());

            //Idioma
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("es-VE");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            Response.TrySkipIisCustomErrors = true;

            //Escribir el log del error
            ILogger logger = DependencyResolver.Current.GetService<ILogger>();
            logger.Log(ex.ToString());

            Response.Write("{'Error': 'true', 'ErrorMessage': 'There was some kind of error and that is all I know'}");
            Server.ClearError();
            Response.End();
        }

    }
}
