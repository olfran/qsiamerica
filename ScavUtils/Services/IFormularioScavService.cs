﻿using System.Collections.Generic;
using ScavUtils.Infrastructure.Models;

namespace ScavUtils.Services
{
    /// <summary>
    /// Servicio para los formulario de SCAV/AIRE
    /// </summary>
    public interface IFormularioScavService
    {
        IList<Campo> GetCamposFormulario(int tipoFormulario);
    }
}