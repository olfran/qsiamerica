﻿using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Globalization;

namespace ScavUtils.Infrastructure.DataAccess
{
    /// <summary>
    /// Utilidades para trabajar con SQL para SCAV con las clases OleDb-*
    /// </summary>
    public class SQLCommonsScav : ISQLCommonsScav
    {
        private string _connectionString = "";
        public string ConnectionString { get; private set; }

        /// <summary>
        /// Paginador, si es null no se paginan los resultados
        /// </summary>
        public Paginador Paginador { get; set; }

        /// <summary>
        /// Constructor que pide un paginador, si se deja en null no se paginan los resultados
        /// </summary>
        public SQLCommonsScav(string connectionString, Paginador paginador = null)
        {
            _connectionString = ConnectionString = connectionString;
            Paginador = paginador;
        }

        /// <summary>
        /// Ejecuta un comando SQL SELECT y llama a un callback con callback(error, SqlDataReader),
        /// tipo Node.js, pero sin async
        /// </summary>
        public bool ExecuteQuery(string sql, Dictionary<string, object> parametros, Action<bool, IDataReader> callback)
        {
            try
            {
                using (OleDbConnection connection = new OleDbConnection(_connectionString))
                {
                    using (OleDbCommand command = new OleDbCommand(sql, connection))
                    {
                        if (parametros != null)
                        {
                            foreach (var item in parametros)
                            {
                                command.Parameters.AddWithValue(item.Key, item.Value);
                            }
                        }
                        connection.Open();
                        OleDbDataReader reader = command.ExecuteReader();
                        callback(false, reader);
                    }
                }
                return true;
            }
            catch
            {
                callback(true, null);

                return false;
            }
        }

        /// <summary>
        /// Ejecutar una sentencia tipo INSERT, DELETE o UPDATE
        /// </summary>
        /// <param name="sql">Sentencia SQL</param>
        /// <param name="parametros">(Opcional) Diccionario con los parámetros para la consulta</param>
        public bool ExecuteNonQuery(string sql, Dictionary<string, object> parametros = null)
        {
            try
            {
                using (OleDbConnection connection = new OleDbConnection(_connectionString))
                {
                    using (OleDbCommand command = new OleDbCommand(sql, connection))
                    {
                        if (parametros != null)
                        {
                            foreach (var item in parametros)
                            {
                                command.Parameters.AddWithValue(item.Key, item.Value);
                            }
                        }
                        connection.Open();

                        return command.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Ejecuta y devuelve un valor escalar, retorna null si hubo error
        /// </summary>
        public object ExecuteScalar(string sql, Dictionary<string, object> parametros = null)
        {
            try
            {
                using (OleDbConnection connection = new OleDbConnection(_connectionString))
                {
                    using (OleDbCommand command = new OleDbCommand(sql, connection))
                    {
                        if (parametros != null)
                        {
                            foreach (var item in parametros)
                            {
                                command.Parameters.AddWithValue(item.Key, item.Value);
                            }
                        }
                        connection.Open();

                        return command.ExecuteScalar();
                    }
                }
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Retorna el SQL SELECT al Catálogo indicado y opcionalmente (si idDocumento > 0) al Documento 
        /// </summary>
        public string GetSelect(IList<Campo> campos, long idCatalogo, long idDocumento = 0, bool incluirForm = true)
        {
            StringBuilder resultado = new StringBuilder();
            string camposSql = GetCamposSeparados(campos, true);
            string coma = !String.IsNullOrEmpty(camposSql) ? "," : "";
            string from = incluirForm ? String.Format("FROM CATALOGO_{0}", idCatalogo) : "";

            //Como el parámetro es long, no hay peligro de SQL Injection
            resultado.AppendFormat("SELECT CATALOGO_{2}.ID_DOCUMENTO,CATALOGO_{2}.NUMREF,CATALOGO_{2}.FECHA_GENERACION {0} {1} {3} ", coma, camposSql, idCatalogo, from);

            if (idDocumento > 0)
            {
                resultado.AppendFormat("WHERE CATALOGO_{0}.ID_DOCUMENTO={1} ", idCatalogo, idDocumento);
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Retorna el SQL INSERT al Catálogo indicado con los campos dinámicos y los parámetros 
        /// correspondientes en forma de ? ideal para ser utilizado en conjunto con los métodos
        /// GetParametros y ExecuteNonQuery
        /// </summary>
        public string GetInsert(IList<Campo> campos, long idCatalogo, string numref, bool incluirVacios = false)
        {
            string sqlInsert = "INSERT INTO CATALOGO_{0}(NUMREF,FECHA_GENERACION,QUETIENE,ESTATUS,ID_USUARIO,{1}) VALUES('{2}','{3}','00000000',0,{4},{5})";
            long idUsuario = GetIdPrimerUsuario();
            string sql = String.Format(sqlInsert,
                idCatalogo,
                GetCamposSeparados(campos, incluirVacios), 
                numref,
                DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"),
                idUsuario,
                GetParametrosSeparados(campos, incluirVacios));

            return sql;
        }

        /// <summary>
        /// Retorna el SQL UPDATE al Catálogo indicado con los campos dinámicos y los parámetros 
        /// correspondientes en forma de ? ideal para ser utilizado en conjunto con los métodos
        /// GetParametros y ExecuteNonQuery
        /// </summary>
        public string GetUpdate(IList<Campo> campos, long idCatalogo, long idDocumento)
        {
            string sqlUpdate = "UPDATE CATALOGO_{0} SET {1} WHERE ID_DOCUMENTO={2}";
            string sql = String.Format(sqlUpdate, idCatalogo, GetCamposUpdate(campos), idDocumento);

            return sql;
        }

        /// <summary>
        /// Retorna el SQL UPDATE al Catálogo indicado con los campos dinámicos para el Where y los parámetros 
        /// correspondientes en forma de ? ideal para ser utilizado en conjunto con los métodos
        /// GetParametros y ExecuteNonQuery
        /// </summary>
        public string GetUpdate(IList<Campo> campos, long idCatalogo, IList<Campo> camposWhere)
        {
            string sqlUpdate = "UPDATE CATALOGO_{0} SET {1} WHERE {2}";
            string sql = String.Format(sqlUpdate, idCatalogo, GetCamposUpdate(campos), GetCamposUpdateWhere(camposWhere));

            return sql;
        }

        /// <summary>
        /// Retorna la parte WHERE para una sentencia UPDATE
        /// </summary>
        private string GetCamposUpdateWhere(IList<Campo> camposWhere)
        {
            StringBuilder resultado = new StringBuilder();

            foreach (Campo campo in camposWhere)
            {
                resultado.AppendFormat("{0}=? AND ", campo.Nombre);
            }

            if (resultado.Length > 0)
            {
                resultado.Remove(resultado.Length - 4, 4);
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Retorna los campos para una sentencia UPDATE con parámetros, CAMPO_1=?,CAMPO_2=?...
        /// </summary>
        private string GetCamposUpdate(IList<Campo> campos)
        {
            StringBuilder resultado = new StringBuilder();

            foreach (Campo campo in campos)
            {
                resultado.AppendFormat("{0}=?,", campo.Nombre);
            }

            if (resultado.Length > 0)
            {
                resultado.Remove(resultado.Length - 1, 1);
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Retorna la sentencia where separada por comas junto con los respectivos parámetros
        /// en forma de ?, ejemplo WHERE CAMPO_1=? AND...
        /// Opcionalmente se puede especificar el operador lógico a utilizar por defecto AND
        /// Por defecto los campos sin valor se omiten, se puede pasar incluirVacios a true para incluirlos
        /// Si ningún campo tiene algún valor se retorna una cadena vacía
        /// </summary>
        /// <param name="campos">Campos a buscar</param>
        /// <param name="operadorLogico">Operador lógico entre AND o OR</param>
        /// <param name="incluirWhere">Si se incluye o no la palabra WHERE de SQL, ideal para filtros</param>
        public string GetWhere(IList<Campo> campos, string operadorLogico = "AND", bool incluirWhere = true, bool incluirVacios = false)
        {
            return ProcesarGetWhere("{0}=? {1} ", campos, operadorLogico, incluirWhere, incluirVacios);
        }

        /// <summary>
        /// Retorna la sentencia where separada por comas junto con los respectivos parámetros
        /// en forma de ?, ejemplo WHERE CAMPO_1 LIKE '%' + ? + '%' AND
        /// Opcionalmente se puede especificar el operador lógico a utilizar por defecto AND
        /// Por defecto los campos sin valor se omiten, se puede pasar incluirVacios a true para incluirlos
        /// Si ningún campo tiene algún valor se retorna una cadena vacía
        /// </summary>
        /// <param name="campos">Campos a buscar</param>
        /// <param name="operadorLogico">Operador lógico entre AND o OR</param>
        /// <param name="incluirWhere">Si se incluye o no la palabra WHERE de SQL, ideal para filtros</param>
        public string GetWhereLike(IList<Campo> campos, string operadorLogico = "AND", bool incluirWhere = true, bool incluirVacios = false)
        {
            return ProcesarGetWhere("{0} LIKE '%' + ? + '%' {1} ", campos, operadorLogico, incluirWhere, incluirVacios);
        }

        private string ProcesarGetWhere(string formato, IList<Campo> campos, string operadorLogico, bool incluirWhere, bool incluirVacios)
        {
            StringBuilder resultado = new StringBuilder(" WHERE (");
            bool hayValor = false;

            if (!incluirWhere)
            {
                resultado.Clear();
            }

            foreach (Campo campo in campos)
            {
                //Omitir campos sin valor
                if (!String.IsNullOrEmpty(campo.Valor) || incluirVacios)
                {
                    if (!incluirWhere && resultado.Length == 0)
                    {
                        resultado.Append(" AND (");
                    }

                    hayValor = true;

                    switch (campo.Tipo)
                    {
                        case TipoCampo.Numref:
                            resultado.AppendFormat(formato, "NUMREF", operadorLogico);
                            break;

                        case TipoCampo.Fecha:
                            resultado.AppendFormat(formato, String.Format("CONVERT(VARCHAR, {0}, 120)", campo.Nombre), operadorLogico);
                            break;

                        default:
                            resultado.AppendFormat(formato, campo.Nombre, operadorLogico);
                            break;
                    }
                }
            }

            if (!hayValor)
            {
                return "";
            }

            if (resultado.Length > 0)
            {
                resultado.Remove(resultado.Length - operadorLogico.Length - 1, operadorLogico.Length);
                resultado.Append(") ");
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Retorna los campos de una lista de nombres de campos separados por comas. Ej. CAMPO_1,CAMPO_2,CAMPO_3
        /// </summary>
        public string GetCamposSeparados(IList<Campo> campos, bool incluirVacios = false)
        {
            StringBuilder resultado = new StringBuilder();

            foreach (Campo campo in campos)
            {
                if (campo.Tipo != TipoCampo.Numref)
                {
                    if (!String.IsNullOrEmpty(campo.Valor))
                    {
                        resultado.AppendFormat("CAMPO_{0},", campo.Id);
                    }
                    else
                    {
                        if (incluirVacios)
                        {
                            resultado.AppendFormat("CAMPO_{0},", campo.Id);
                        }
                    }
                }
            }

            if (resultado.Length > 0)
            {
                resultado.Remove(resultado.Length - 1, 1);
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Retorna un diccionario de parámetros NombreCampo / Valor
        /// </summary>
        /// <param name="campos">Campos con sus respectivos valores</param>
        /// <param name="valor">Opcional: Valor a utilizar en lugar de los valores de los campos</param>
        /// <param name="comenzarDesde">Valor desde el cual comenzar a construir los parámetros</param>
        /// <param name="incluirVacios">Incluir o no los campos vacíos</param>
        /// <returns>Diccionario de parámetros NombreCampo / Valor</returns>
        public Dictionary<string, object> GetParametros(IList<Campo> campos, string valor = "", int comenzarDesde = 1, bool incluirVacios = false)
        {
            Dictionary<string, object> resultado = new Dictionary<string, object>();
            int counter = comenzarDesde;

            foreach (Campo campo in campos)
            {
                string valorCampo = String.IsNullOrEmpty(valor) ? campo.Valor : valor;

                if (!String.IsNullOrEmpty(valorCampo) || incluirVacios)
                {
                    string valorFinal = valorCampo;

                    if (campo.Tipo == TipoCampo.Fecha)
                    {
                        DateTime? fecha = ParseDate(valorFinal);

                        if (fecha.HasValue)
                        {
                            valorFinal = fecha.Value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                        }
                    }

                    resultado.Add(String.Format("@p{0}", counter++), valorFinal);
                }
            }

            return resultado;
        }

        /// <summary>
        /// Retorna los campos de una lista de Parámetros separados por comas. Ej. ?,?,?
        /// </summary>
        public string GetParametrosSeparados(IList<Campo> campos, bool incluirVacios = false)
        {
            StringBuilder resultado = new StringBuilder();

            foreach (Campo campo in campos)
            {
                if (campo.Tipo != TipoCampo.Numref)
                {
                    if (!String.IsNullOrEmpty(campo.Valor))
                    {
                        resultado.Append("?,");
                    }
                    else
                    {
                        if (incluirVacios)
                        {
                            resultado.Append("?,");
                        }
                    }
                }
            }

            if (resultado.Length > 0)
            {
                resultado.Remove(resultado.Length - 1, 1);
            }

            return resultado.ToString();
        }

        public long GetIdPrimerUsuario()
        {
            return ParseLong(ExecuteScalar("SELECT TOP 1 ID_USUARIO FROM USUARIO").ToString());
        }

        /// <summary>
        /// Retorna el SQL para la paginación, sólo funciona en SQL Server 2012
        /// </summary>
        /// <param name="orderBy">
        /// bool: Si se anexa order by a la consulta, ya que es requerido por SQL Server
        /// </param>
        public string GetSqlPaginador(bool orderBy = true)
        {
            if (Paginador == null)
            {
                return "";
            }
            string orderBySql = orderBy ? " ORDER BY ID_DOCUMENTO " : "";
            
            return String.Format(" {0} OFFSET {1} ROWS FETCH NEXT {2} ROWS ONLY ", 
                orderBySql,
                Paginador.Offset, 
                Paginador.TamanoPagina);
        }

        /// <summary>
        /// Convierte un valor string a Date
        /// </summary>
        /// <param name="str">Valor a convertir</param>
        /// <returns>Valor convertido o DateTime.Now si hubo error</returns>
        public DateTime? ParseDate(string str)
        {
            try
            {
                return DateTime.Parse(str);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Convierte un valor string a long
        /// </summary>
        /// <param name="number">Valor a convertir</param>
        /// <returns>Valor convertido o 0 si hubo error</returns>
        public long ParseLong(string number)
        {
            long num = 0;
            long.TryParse(number, out num);
            return num;
        }

        /// <summary>
        /// Normaliza las fechas a formato ISO para SQL
        /// </summary>
        public void NormalizarFechas(IList<Campo> campos)
        {
            foreach (var campo in campos)
            {
                if (campo.Tipo == TipoCampo.Fecha)
                {
                    DateTime? fecha = ParseDate(campo.Valor);

                    campo.Valor = fecha.HasValue ? fecha.Value.ToString("yyyy-MM-dd HH:mm:ss") : "";
                }
            }
        }

        public IList<Campo> MapReaderToCampos(IList<Campo> campos, IDataReader reader)
        {
            IList<Campo> resultado = new List<Campo>();

            foreach (Campo campo in campos)
            {
                try
                {
                    Campo campoAux = new Campo()
                    {
                        Id = campo.Id,
                        Tipo = campo.Tipo,
                        IdCatalogo = campo.IdCatalogo,
                        Valor = reader[campo.Nombre].ToString()
                    };

                    resultado.Add(campoAux);
                }
                catch
                {
                    continue;
                }
            }
    
            return resultado;
        }
    }
}