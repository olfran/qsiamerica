﻿using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;

namespace ScavUtils.Infrastructure.DataAccess.Adapters
{
    public class AdjuntoDataAccess : IAdjuntoDataAccess
    {
        readonly ISQLCommonsScav _sqlCommons;

        public AdjuntoDataAccess(ISQLCommonsScav sqlCommons)
        {
            _sqlCommons = sqlCommons;
        }

        /// <summary>
        /// Retorna todos los adjuntos pertenecientes al documento
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <returns>Lista o lista vacía si no hay ningún adjunto</returns>
        public IList<AdjuntoPropiedades> GetAdjuntos(long idCatalogo, long idDocumento)
        {
            IList<AdjuntoPropiedades> resultado = new List<AdjuntoPropiedades>();
            string joinCatalogo = String.Format(" JOIN CATALOGO_{0} ON CATALOGO_{0}.ID_DOCUMENTO=ADJUNTOS_C_{0}.ID_DOCUMENTO", idCatalogo);
            string sqlWhere = String.Format(" WHERE FECHA_ELIMINACION IS NULL AND ADJUNTOS_C_{0}.ID_DOCUMENTO={1}", idCatalogo, idDocumento);
            string sqlOrderBy = " ORDER BY FECHA_ASOCIACION ASC";
            string sql = String.Format("SELECT ADJUNTOS_C_{0}.NOMBRE_ARCH,ADJUNTOS_C_{0}.ID_ADJUNTO,ADJUNTOS_C_{0}.NOMBRE_PANTA,ADJUNTOS_C_{0}.EXT FROM ADJUNTOS_C_{0} {1} JOIN USUARIO ON USUARIO.ID_USUARIO=ADJUNTOS_C_{0}.ID_USUARIO ", idCatalogo, joinCatalogo);

            _sqlCommons.ExecuteQuery(sql + sqlWhere + sqlOrderBy, null, (error, reader) => {
                if (!error)
                {
                    while (reader.Read())
                    {
                        resultado.Add(new AdjuntoPropiedades()
                        {
                            Id = _sqlCommons.ParseLong(reader[1].ToString()),
                            Nombre = reader[2].ToString(),
                            NombreFisico = reader[0].ToString(),
                            Extension = reader[3].ToString()
                        });

                    }
                }
            });

            return resultado;
        }

        /// <summary>
        /// Encuentra un adjunto
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <param name="idAdjunto">Id adjunto</param>
        /// <returns>Datos del adjunto</returns>
        public AdjuntoPropiedades FindAdjunto(long idCatalogo, long idDocumento, long idAdjunto)
        {
            AdjuntoPropiedades resultado = new AdjuntoPropiedades();
            string joinCatalogo = String.Format(" JOIN CATALOGO_{0} ON CATALOGO_{0}.ID_DOCUMENTO=ADJUNTOS_C_{0}.ID_DOCUMENTO", idCatalogo);
            string sqlWhere = String.Format(" WHERE FECHA_ELIMINACION IS NULL AND ADJUNTOS_C_{0}.ID_DOCUMENTO={1} AND ADJUNTOS_C_{0}.ID_ADJUNTO={2}", idCatalogo, idDocumento, idAdjunto);
            string sqlOrderBy = " ORDER BY FECHA_ASOCIACION ASC";
            string sql = String.Format("SELECT ADJUNTOS_C_{0}.NOMBRE_ARCH,ADJUNTOS_C_{0}.ID_ADJUNTO,ADJUNTOS_C_{0}.NOMBRE_PANTA,ADJUNTOS_C_{0}.EXT FROM ADJUNTOS_C_{0} {1} JOIN USUARIO ON USUARIO.ID_USUARIO=ADJUNTOS_C_{0}.ID_USUARIO ", idCatalogo, joinCatalogo);

            _sqlCommons.ExecuteQuery(sql + sqlWhere + sqlOrderBy, null, (error, reader) => {
                if (!error)
                {
                    reader.Read();
                    resultado.Id = _sqlCommons.ParseLong(reader[1].ToString());
                    resultado.Nombre = reader[2].ToString();
                    resultado.NombreFisico = reader[0].ToString();
                    resultado.Extension = reader[3].ToString();
                }
            });

            return resultado;
        }
    }
}
