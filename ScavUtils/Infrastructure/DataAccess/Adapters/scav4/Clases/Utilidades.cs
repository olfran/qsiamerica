﻿using System;

namespace scav4.Clases
{
    public static class Utilidades
    {
        /// <summary>
        /// Retorna la fecha actual formateada para SQL Server, INCLUYENDO las comillas simples o literal de fecha
        /// </summary>
        /// <param name="horaMaxima">Indica si se trae la hora máxima de un día: 23:59:59 (para los casos de comparación de fecha (límite superior hasta los momentos))</param>
        /// <returns></returns>
        public static string getFechaActualSQL(bool horaMaxima = false)
        {
            if (!horaMaxima)
            {
                return "'" + string.Format("{0:yyyyMMdd HH:mm:ss}", DateTime.Now) + "'";
            }
            else
            {
                return "'" + string.Format("{0:yyyyMMdd}", DateTime.Now) + " 23:59:59'";
            }
        }

        /// <summary>
        /// Agrega la cantidad de ceros faltantes a un valor segun el formato (cantidad de ceros)
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="cantidadDeCeros"></param>
        /// <param name="parteDerecha">Para valores decimales agrega ceros a la derecha, false por defecto</param>
        /// <returns></returns>
        public static string rellenaCeros(string valor, int cantidadDeCeros, bool parteDerecha = false)
        {
            string nuevoValor = valor;
            if (valor.Length < cantidadDeCeros)
            {
                for (int i = valor.Length; i < cantidadDeCeros; i++)
                {
                    if (parteDerecha) { nuevoValor = nuevoValor + "0"; }
                    else
                    {
                        nuevoValor = "0" + nuevoValor;
                    }
                }
            }
            return nuevoValor;
        }

        /// <summary>
        /// Normaliza la máscara de un campo fecha o número (actualmente sólo para fecha) proveniente de la versión 3.0
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string getNomralizaMascara(string valor)
        {
            string resultado = "";
            switch (valor.ToLower())
            {
                case "dd/mm/aaaa":
                    resultado = "dd/MM/yyyy";
                    break;
                case "dd/mm/aa":
                    resultado = "dd/MM/yy";
                    break;
                case "mm/dd/aaaa":
                    resultado = "MM/dd/yyyy";
                    break;
                case "mm/dd/aa":
                    resultado = "MM/dd/yy";
                    break;
                case "hora":
                    resultado = "hh:mm";
                    break;
                case "hora con seg.":
                    resultado = "hh:mm:ss";
                    break;
                case "dd/mm/aaaa hh:mm":
                    resultado = "dd/MM/yyyy hh:mm";
                    break;
                case "dd/mm/aaaa hh:mm:ss":
                    resultado = "dd/MM/yyyy hh:mm:ss";
                    break;
                case "00,0":
                case "00,00":
                case "0.000,0":
                case "0.000,00":
                    resultado = valor;
                    break;
            }
            return resultado;
        }

        /// <summary>
        /// Para cambiar una variable bool a inetera
        /// <para>Milko 10/02/2014</para>
        /// </summary>
        /// <param name="valor">Valor true o false</param>
        /// <returns>Devuelve 1 si es true o 0 si es false</returns>
        public static int getVerdaderoFalso(bool valor)
        {
            if (valor) { return 1; } else { return 0; }
        }
        /// <summary>
        /// Devuleve el valor long de un string, controlando el error de conversión
        /// <para>Milko Rivas 01/12/2012</para>
        /// </summary>
        /// <param name="valor">Valor string a evaluar</param>
        /// <returns>Valor long o cero si da error</returns>
        public static long getValorLongDeString(string valor)
        {
            if (valor == "" || valor == null)
            {
                return 0;
            }
            else
            {
                long valorDefecto = 0;
                long.TryParse(valor, out valorDefecto);
                return valorDefecto;
            }
        }
        /// <summary>
        /// Devuleve el valor int de un string, controlando el error de conversión
        /// <para>Milko Rivas 13/03/2013</para>
        /// </summary>
        /// <param name="valor">Valor string a evaluar</param>
        /// <returns>Valor int o cero si da error</returns>
        public static int getValorIntDeString(string valor)
        {
            if (valor == "" || valor == null)
            {
                return 0;
            }
            else
            {
                int valorDefecto = 0;
                int.TryParse(valor, out valorDefecto);
                return valorDefecto;
            }
        }

        /// <summary>
        /// Para determinar si un valor numérico es verdadero o falso
        /// <para>Milko 28/11/2012</para>
        /// </summary>
        /// <param name="valor">Objeto tipo entero o string</param>
        /// <returns>Devuelve verdadero si el objeto es igual a "1"</returns>
        public static bool getVerdaderoFalso(object valor)
        {
            return valor.ToString() == "1";
        }

        /// <summary>
        /// Devuelve la fecha actual en formato yyyy/MM/dd hh:mm:ss
        /// </summary>
        /// <returns></returns>
        public static string getFechaActual()
        {
            return getFechaActual('/', ':');
        }

        /// <summary>
        /// Devuelve la fecha actual con el formato especificado
        /// </summary>
        /// <param name="formato"></param>
        /// <returns></returns>
        public static string getFechaActual(string formato)
        {
            return string.Format("{0:" + formato + "}", DateTime.Now);
        }

        public static string getFechaActual(char separadorDiaMesAno, char separadorHoraMinSeg, bool omitirHora = false)
        {
            if (omitirHora)
            {
                return string.Format("{0:yyyy" + separadorDiaMesAno + "MM" + separadorDiaMesAno + "dd}", DateTime.Now);
            }
            else
            {
                return string.Format("{0:yyyy" + separadorDiaMesAno + "MM" + separadorDiaMesAno + "dd HH" + separadorHoraMinSeg + "mm" + separadorHoraMinSeg + "ss}", DateTime.Now);
            }
        }
    }
}