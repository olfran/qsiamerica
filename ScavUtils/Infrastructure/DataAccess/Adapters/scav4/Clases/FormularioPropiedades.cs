﻿
namespace scav4.Clases
{
    /// <summary>
    /// get / set para las propiedades del formulario
    /// </summary>
    public class FormularioPropiedades
    {
        public long idFormulario { get; set; }
        public string nombreFormulario { get; set; }
        public int _filas { get; set; }
        public int _columnas { get; set; }
        public int _largoPapel { get; set; }
        public int _anchoPapel { get; set; }
        public int _margen_iz { get; set; }
        public int _margen_de { get; set; }
        public int _margen_sup { get; set; }
        public int _margen_inf { get; set; }
        //public string idCatalogo { get; set; }

    }
}