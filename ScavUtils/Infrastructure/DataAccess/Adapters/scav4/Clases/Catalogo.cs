﻿using System;
using System.Linq;
using System.Data.OleDb;

namespace scav4.Clases
{
    public class Catalogo
    {
        private readonly string _connectionString;

        public Catalogo(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Devulve el próximo número de referencia
        /// </summary>
        /// <param name="idPrefijo"></param>
        /// <param name="idCatalogo"></param>
        /// <returns></returns>
        public string getGeneraNumref(long idPrefijo, long idCatalogo, string anio ="")
        {
            try
            {
                using (OleDbConnection cn = new OleDbConnection(_connectionString))
                {
                    cn.Open();
                    Prefijo prefijoCarga = new Prefijo(_connectionString);
                    PrefijoPropiedades prefijo;
                    if (idPrefijo > 0)
                    {
                        prefijo = prefijoCarga.getPrefijo(idPrefijo);
                    }
                    else {
                        idPrefijo=prefijoCarga.getListaPrefijos(idCatalogo).First().idPrefijo; //busco el primer prefijo
                        prefijo = prefijoCarga.getPrefijo(idPrefijo);
                    }
                    string ano = "";
                    string nombrePrefijo = prefijo.nombrePrefijo;
                    int digitosAno = prefijo.digitosAno;
                    int digitosConsec = prefijo.digitosConsecutivo;
                    if (digitosAno == 4) { ano = DateTime.Now.Year.ToString(); } else { ano = DateTime.Now.Year.ToString().Substring(2, 2); }
                    if (anio != "") { ano = anio; }
                    long consecutivo = 0;
                    OleDbCommand sqlCmd = new OleDbCommand("SELECT MAX(NUMREF) FROM CATALOGO_" + idCatalogo + " WHERE NUMREF LIKE ?", cn);
                    sqlCmd.Parameters.Add("@numref", OleDbType.VarChar, 20).Value = prefijo.nombrePrefijo + "-" + ano + "-%";
                    OleDbDataReader sqlReader = sqlCmd.ExecuteReader();
                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();
                        string ultimoNumref = sqlReader.GetValue(0).ToString() + "";
                        if (ultimoNumref != "")
                        {
                            string[] divididos = ultimoNumref.Split('-');
                            consecutivo = long.Parse(divididos[2]);
                        }
                    }
                    consecutivo++;
                    return prefijo.nombrePrefijo + "-" + ano + "-" + Utilidades.rellenaCeros(consecutivo.ToString(), digitosConsec);
                }
            }
            catch { return ""; }
        }
   }
}