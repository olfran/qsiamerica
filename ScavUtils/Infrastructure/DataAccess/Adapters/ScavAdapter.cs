﻿using ScavUtils.Infrastructure.Models;
using scav4.Clases;
using System;
using System.Collections.Generic;

namespace ScavUtils.Infrastructure.DataAccess.Adapters
{
    /// <summary>
    /// Clase Adapter con todos los métodos de SCAV utilizados
    /// </summary>
    public class ScavAdapter : IScavAdapter
    {
        ISQLCommonsScav _sqlCommons;
        ITesauroDataAccess _tesauroDataAccess;

        public ScavAdapter(ISQLCommonsScav sqlCommons)
        {
            _sqlCommons = sqlCommons;
            _tesauroDataAccess = null;
        }

        public ScavAdapter(ISQLCommonsScav sqlCommons, ITesauroDataAccess tesauroDataAccess)
        {
            _sqlCommons = sqlCommons;
            _tesauroDataAccess = tesauroDataAccess;
        }

        /// <summary>
        /// Retorna el ID del primer prefijo correspondiente al Catálogo deseado
        /// </summary>
        /// <returns>ID del promer prefijo o 0 si hubo error</returns>
        /// <param name="idCatalogo">Id del catálogo</param>
        public long GetIdPrimerPrefijo(long idCatalogo)
        {
            long resultado = 0;
            string sql = "SELECT ID_PREFIJO FROM PREFIJO WHERE ID_CATALOGO=?";
            Dictionary<string, object> parametros = new Dictionary<string,object>()
            {
                { "@p1", idCatalogo }
            };
            resultado = _sqlCommons.ParseLong(_sqlCommons.ExecuteScalar(sql, parametros).ToString());

            return resultado;
        }

        /// <summary>
        /// Generar el próximo Numref
        /// </summary>
        public string GenerarNumref(long idCatalogo, long idPrefijo)
        {
            Catalogo catalogo = new Catalogo(_sqlCommons.ConnectionString);
            return catalogo.getGeneraNumref(idPrefijo, idCatalogo);
        }

        /// <summary>
        /// Retorna los campos de un formulario dependiendo del tipo, por defecto NO carga
        /// los Tesauros y sus Términos para optimizar el resultado, pero se puede optar pasar
        /// true en cargarTesauros para cargarlos
        /// </summary>
        public IList<Campo> GetCamposFormulario(long idCatalogo, int tipoFormulario = 1, bool cargarTesauros = false)
        {
            try
            {
                Formulario formulario = new Formulario(_sqlCommons.ConnectionString);
                IList<FormularioPropiedades> formularios = formulario.getFormularioLista(idCatalogo, tipoFormulario);

                long idFormularioRegistro = 0;

                if (formularios != null && formularios.Count > 0)
                {
                    idFormularioRegistro = formularios[0].idFormulario;
                }

                IList<FormularioDetallePropiedades> campos = formulario.getFormularioDetalle(idCatalogo, idFormularioRegistro);
                return MapToCampoModel(idCatalogo, campos, cargarTesauros);
            }
            catch
            {
                return new List<Campo>();
            }
        }

        /// <summary>
        /// Mapear al Modelo de Campo
        /// </summary>
        private IList<Campo> MapToCampoModel(long idCatalogo, IList<FormularioDetallePropiedades> campos, bool cargarTesauros = false)
        {
            IList<Campo> resultado = new List<Campo>();
            IList<long> camposUnicos = GetCamposUnicos(idCatalogo);

            foreach (var campo in campos)
            {
                if (campo._tipoElemento == scav4.Clases.TipoCampo.Fecha || 
                    campo._tipoElemento == scav4.Clases.TipoCampo.Memo ||
                    campo._tipoElemento == scav4.Clases.TipoCampo.Numero ||
                    campo._tipoElemento == scav4.Clases.TipoCampo.Numref ||
                    campo._tipoElemento == scav4.Clases.TipoCampo.Texto)
                {
                    Campo nuevoCampo = new Campo()
                    {
                        Id = campo._idCampo,
                        Titulo = campo._nombreCampo,
                        Tipo = campo._tipoElemento,
                        Obligatorio = campo._obligatorio,
                        LonguitudMaxima = campo._MaxLenght,
                        ValorDefecto = campo.ValorDefecto,
                        IdCatalogo = idCatalogo,
                        Unico = camposUnicos.Contains(campo._idCampo)
                    };

                    if (campo._idTesauro > 0 && cargarTesauros)
                    {
                        nuevoCampo.Tesauro = _tesauroDataAccess.GetTerminos(campo._idTesauro);
                    }

                    resultado.Add(nuevoCampo);
                }
            }

            return resultado;
        }
        
        /// <summary>
        /// Retorna los campos únicos del Catálogo o una lista vacía si hubo error
        /// </summary>
        IList<long> GetCamposUnicos(long idCatalogo)
        {
            String sql = String.Format("SELECT ID_CAMPO_CAT_STR FROM CATALOGO_ESTRUCTURA WHERE ID_CATALOGO = ? AND UNICO_CAT_STR=1");
            List<long> resultado = new List<long>();
            Dictionary<string, object> parametros = new Dictionary<string,object>()
            {
                { "@p1", idCatalogo }
            };

            _sqlCommons.ExecuteQuery(sql, parametros, (error, reader) =>
            {
                if (!error)
                {
                    while (reader.Read())
                    {
                        resultado.Add(_sqlCommons.ParseLong(reader["ID_CAMPO_CAT_STR"].ToString()));
                    }
                }
            });

            return resultado;
        }

        /// <summary>
        /// Permite enviar un Flujo de Trabajo
        /// </summary>
        public bool EnviarFlujo(long idCatalogo, long idDocumento)
        {
            //Dummy
            return true;
            /*
            try
            {
                long idUsuario = _sqlCommons.GetIdPrimerUsuario();
                FlujoTrabajo flujoTrabajo = new FlujoTrabajo(_sqlCommons.ConnectionString);
                FlujoEstacion estacionActual = new FlujoEstacion();
                DataTable tabla = flujoTrabajo.getFlujosAsociadosTabla(idCatalogo);
                long idFlujo = _sqlCommons.ParseLong(tabla.Rows[0]["ID_FLUJO"].ToString());
                long idFlujoVersion = flujoTrabajo.getIdFlujoVersion(idFlujo);
                var estaciones = estacionActual.getEstacionesHijas(0, idFlujoVersion, null, idDocumento, idCatalogo, idFlujo, idUsuario, true);

                FlujoPropiedadesEnvio flujoPropiedadesEnvio = new FlujoPropiedadesEnvio();
                flujoPropiedadesEnvio._idCatalogo = idCatalogo;
                flujoPropiedadesEnvio._idDocumento = idDocumento;
                flujoPropiedadesEnvio._idFlujoVersion = idFlujoVersion;
                flujoPropiedadesEnvio._idFlujo = idFlujo;
                flujoPropiedadesEnvio._respuesta = "";
                flujoPropiedadesEnvio._estaciones = estaciones;
                flujoPropiedadesEnvio._estacionInicial = true;

                return flujoTrabajo.setContinuaFlujo(idUsuario, flujoPropiedadesEnvio);
            }
            catch
            {
                return false;
            }
            */
        }
    }
}
