﻿using System.Collections.Generic;
using ScavUtils.Infrastructure.Models;

namespace ScavUtils.Infrastructure.DataAccess.Adapters
{
    public interface IAdjuntoDataAccess
    {
        /// <summary>
        /// Retorna todos los adjuntos pertenecientes al documento y al usuario especificado
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <returns>Lista o lista vacía si no hay ningún adjunto</returns>
        IList<AdjuntoPropiedades> GetAdjuntos(long idCatalogo, long idDocumento);

        /// <summary>
        /// Encuentra un adjunto
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <param name="idDocumento">Id documento</param>
        /// <param name="idAdjunto">Id adjunto</param>
        /// <returns>Datos del adjunto</returns>
        AdjuntoPropiedades FindAdjunto(long idCatalogo, long idDocumento, long idAdjunto);
    }
}