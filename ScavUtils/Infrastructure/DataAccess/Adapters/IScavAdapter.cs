﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;

namespace ScavUtils.Infrastructure.DataAccess.Adapters
{
    public interface IScavAdapter
    {
        /// <summary>
        /// Retorna el ID del primer prefijo correspondiente al Catálogo deseado
        /// </summary>
        /// <returns>ID del promer prefijo o 0 si hubo error</returns>
        long GetIdPrimerPrefijo(long idCatalogo);

        /// <summary>
        /// Generar el próximo Numref
        /// </summary>
        string GenerarNumref(long idCatalogo, long idPrefijo); 

        /// <summary>
        /// Retorna los campos de un formulario dependiendo del tipo, por defecto NO carga
        /// los Tesauros y sus Términos para optimizar el resultado, pero se puede optar pasar
        /// true en cargarTesauros para cargarlos
        /// </summary>
        IList<Campo> GetCamposFormulario(long idCatalogo, int tipoFormulario, bool cargarTesauros = false);

        /// <summary>
        /// Permite enviar un Flujo de Trabajo
        /// </summary>
        bool EnviarFlujo(long idCatalogo, long idDocumento);
    }
}
