﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;

namespace ScavUtils.Infrastructure.DataAccess
{
    public interface IScavDataAccess
    {
        /// <summary>
        /// Crear un Documento en el catálogo definido en el constructor
        /// </summary>
        /// <returns>ID_DOCUMENTO si todo fue bien de lo contrario 0</returns>
        long CrearDocumento(IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Actualizar un Documento
        /// </summary>
        /// <returns>true si todo fue bien de lo contrario false</returns>
        bool ActualizarDocumento(IList<Campo> campos, long idCatalogo, long idDocumento);
        bool ActualizarDocumento(IList<Campo> campos, long idCatalogo, IList<Campo> camposWhere);

        /// <summary>
        /// Retorna los "Filtros" correspondientes a cada campo ordenados de manera descendiente
        /// los filtros no son más que campos agrupados en una sentencia SQL junto con el total
        /// de datos de ese campo
        /// </summary>
        /// <summary>
        /// Retorna los campos de un formulario dependiendo del tipo, o una lista vacía si hubo error
        /// </summary>
        IList<Campo> GetCamposFormulario(long idCatalogo, int tipoFormulario = 1, bool cargarTesauros = false);

        /// <summary>
        /// Retorna el total de registros de un Catálogo de manera eficiente
        /// </summary>
        long GetTotal(long idCatalogo, IList<Campo> camposFiltro = null);

        /// <summary>
        /// Retorna el total de registros de una búsqueda de manera eficiente
        /// </summary>
        long GetTotalBuscar(long idCatalogo, IList<Campo> camposFiltro);

        /// <summary>
        /// Retorna el total de registros de un Catálogo de manera eficiente, pensado para las búsquedas amplias
        /// </summary>
        long GetTotalBusquedaAmplia(string texto, IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Llenar el Documento con todos los datos
        /// </summary>
        /// <returns>Lista de todos los Documentos con sus respectivos valores o una lista vacía si hubo error</returns>
        Documento GetDocumento(IList<Campo> campos, long idCatalogo, long idDocumento);

        /// <summary>
        /// Trata de encontrar todos los documentos del Catálogo junto con los campos y sus respectivos
        /// valores, si los campos tienen algún valor se crea una consulta WHERE and con sus valores
        /// Los resultados se regresan paginados dependiendo de la configuración del paginador en la dependencia
        /// ISQLCommonsScav
        /// </summary>
        /// <returns>Lista de Documentos con sus respectivos valores o una lista vacía si hubo error</returns>
        IList<Documento> FindDocumentos(IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Reliza una búsqueda, con el valor de cada campo por separado
        /// retorna una lista de documentos o una lista vacía si no hubo resultados
        /// </summary>
        ///<param name="campos">Campos a buscar con sus respectivos valores</param>
        ///<param name="idCatalogo">ID del Catálogo al cual pertenecen los campos</param>
        IList<Documento> Buscar(IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Reliza una búsqueda amplia, sin filtro, en todos los campos del catálogo deseado y 
        /// retorna una lista de documentos o una lista vacía si no hubo resultados
        /// </summary>
        ///<param name="campos">Campos a buscar con sus respectivos valores</param>
        ///<param name="idCatalogo">ID del Catálogo al cual pertenecen los campos</param>
        IList<Documento> BusquedaAmplia(string query, IList<Campo> campos, long idCatalogo);

        /// <summary>
        /// Establecer el paginador
        /// </summary>
        void SetPaginador(Paginador paginador);

        /// <summary>
        /// Permite enviar un Flujo de Trabajo
        /// </summary>
        bool EnviarFlujo(long idCatalogo, long idDocumento);

        /// <summary>
        /// Operador lógico para el método Buscar
        /// 0 == AND 1 == OR, por defecto AND
        /// </summary>
        int OperadorLogicoBuscar { get; set; }
    }
}
