﻿using ScavUtils.Infrastructure.DataAccess.Adapters;
using ScavUtils.Infrastructure.Models;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ScavUtils.Infrastructure.DataAccess
{
    /// <summary>
    /// Acceso a datos de los documentos Scav
    /// </summary>
    public class ScavDataAccess : IScavDataAccess
    {
        protected ISQLCommonsScav _sqlCommons;
        protected IScavAdapter _scavAdapter;

        public ScavDataAccess(ISQLCommonsScav sqlCommons, IScavAdapter scavAdapter)
        {
            _sqlCommons = sqlCommons;
            _scavAdapter = scavAdapter;
        }

        public int OperadorLogicoBuscar { get; set; }

        /// <summary>
        /// Crear un Documento en el Catálogo
        /// </summary>
        /// <returns>ID_DOCUMENTO si todo fue bien de lo contrario 0</returns>
        public long CrearDocumento(IList<Campo> campos, long idCatalogo)
        {
            try
            {
                long idPrefijo = _scavAdapter.GetIdPrimerPrefijo(idCatalogo);
                string numref = _scavAdapter.GenerarNumref(idCatalogo, idPrefijo);
                string sqlInsert = _sqlCommons.GetInsert(campos, idCatalogo, numref);

                Dictionary<string, object> parametros = _sqlCommons.GetParametros(campos);

                //Normalizar Fechas
                _sqlCommons.NormalizarFechas(campos);

                object resultado = 
                    _sqlCommons.ExecuteScalar(sqlInsert + ";SELECT SCOPE_IDENTITY()", parametros);

                return _sqlCommons.ParseLong(resultado.ToString());
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Editar un Documento
        /// </summary>
        /// <returns>true si todo fue bien de lo contrario false</returns>
        public bool ActualizarDocumento(IList<Campo> campos, long idCatalogo, long idDocumento)
        {
            try
            {
                _sqlCommons.NormalizarFechas(campos);
                string sqlUpdate = _sqlCommons.GetUpdate(campos, idCatalogo, idDocumento);
                Dictionary<string, object> parametros = _sqlCommons.GetParametros(campos, "", 1, true);

                return _sqlCommons.ExecuteNonQuery(sqlUpdate, parametros);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Editar un Documento
        /// </summary>
        /// <returns>true si todo fue bien de lo contrario false</returns>
        public bool ActualizarDocumento(IList<Campo> campos, long idCatalogo, IList<Campo> camposWhere)
        {
            List<Campo> mergedCampos = new List<Campo>();

            mergedCampos.AddRange(campos);
            mergedCampos.AddRange(camposWhere);

            try
            {
                _sqlCommons.NormalizarFechas(campos);
                string sqlUpdate = _sqlCommons.GetUpdate(campos, idCatalogo, camposWhere);
                Dictionary<string, object> parametros = _sqlCommons.GetParametros(mergedCampos, "", 1, true);

                return _sqlCommons.ExecuteNonQuery(sqlUpdate, parametros);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Retorna los campos del formulario solicitado o una lista vacía si hubo error.
        /// Se puede utilizar la estructura TipoFormulario para una fácil identificación.
        /// </summary>
        public IList<Campo> GetCamposFormulario(long idCatalogo, int tipoFormulario = 0, bool cargarTesauros = false)
        {
            return _scavAdapter.GetCamposFormulario(idCatalogo, tipoFormulario, cargarTesauros);
        }

        /// <summary>
        /// Reliza una búsqueda, con el valor de cada campo por separado
        /// retorna una lista de documentos o una lista vacía si no hubo resultados
        /// </summary>
        ///<param name="campos">Campos a buscar con sus respectivos valores</param>
        ///<param name="idCatalogo">ID del Catálogo al cual pertenecen los campos</param>
        public IList<Documento> Buscar(IList<Campo> campos, long idCatalogo)
        {
            string select = _sqlCommons.GetSelect(campos, idCatalogo, 0, false);
            string where = _sqlCommons.GetWhereLike(campos, GetOperadorLogicoBuscar(OperadorLogicoBuscar), true);
            string joinAdjuntos = String.Format(" JOIN(SELECT CATALOGO_{0}.ID_DOCUMENTO, COUNT(ADJUNTOS_C_{0}.ID_DOCUMENTO) AS TOTAL_ADJUNTOS FROM CATALOGO_{0} LEFT OUTER JOIN ADJUNTOS_C_{0} ON CATALOGO_{0}.ID_DOCUMENTO=ADJUNTOS_C_{0}.ID_DOCUMENTO WHERE FECHA_ELIMINACION IS NULL GROUP BY CATALOGO_{0}.ID_DOCUMENTO) T ON CATALOGO_{0}.ID_DOCUMENTO=T.ID_DOCUMENTO", idCatalogo);
            string joinFlujo = String.Format(" LEFT OUTER JOIN (SELECT DISTINCT CATALOGO_{0}.ID_DOCUMENTO,ID_FLUJO FROM CATALOGO_{0} LEFT OUTER JOIN FLUJO_CAT_{0} ON CATALOGO_{0}.ID_DOCUMENTO=FLUJO_CAT_{0}.ID_DOCUMENTO WHERE ID_FLUJO IS NOT NULL) F ON F.ID_DOCUMENTO=CATALOGO_{0}.ID_DOCUMENTO ", idCatalogo);
            Dictionary<string, object> parametros = _sqlCommons.GetParametros(campos);
            IList<Documento> resultado = new List<Documento>();
            string sqlSelect = String.Format("{0},T.TOTAL_ADJUNTOS,F.ID_FLUJO FROM CATALOGO_{1} {2} {3} {4} {5}", select, idCatalogo, joinAdjuntos, joinFlujo, where, _sqlCommons.GetSqlPaginador());

            _sqlCommons.ExecuteQuery(sqlSelect, parametros, (error, reader) =>
            {
                if (!error)
                {
                    while (reader.Read())
                    {
                        IList<Campo> camposAux = new List<Campo>();

                        foreach (Campo campo in campos)
                        {
                            string nombreCampo = campo.Nombre;

                            if (reader[nombreCampo] != null)
                            {
                                camposAux.Add(new Campo()
                                {
                                    Id = campo.Id,
                                    Tipo = campo.Tipo,
                                    Valor = reader[nombreCampo].ToString(),
                                    Titulo = campo.Titulo,
                                    IdCatalogo = idCatalogo
                                });
                            }
                        }
                        resultado.Add(new Documento()
                        {
                            Campos = camposAux,
                            Id = ParseLong(reader["ID_DOCUMENTO"].ToString()),
                            IdFlujo = ParseLong(reader["ID_FLUJO"].ToString()),
                            Numref = reader["NUMREF"].ToString(),
                            TotalAdjuntos = ParseUint(reader["TOTAL_ADJUNTOS"].ToString())
                        });
                    }
                }
            });

            return resultado;
        }

        /// <summary>
        /// Reliza una búsqueda amplia, con filtro, en todos los campos del catálogo deseado y 
        /// retorna una lista de documentos o una lista vacía si no hubo resultados
        /// </summary>
        ///<param name="texto">Texto a buscar en cada campo</param>
        ///<param name="campos">Campos a buscar con sus respectivos valores</param>
        ///<param name="idCatalogo">ID del Catálogo al cual pertenecen los campos</param>
        public IList<Documento> BusquedaAmplia(string texto, IList<Campo> campos, long idCatalogo)
        {
            string select = _sqlCommons.GetSelect(campos, idCatalogo, 0, false);
            string whereAmplio = _sqlCommons.GetWhereLike(campos, "OR", true, true);
            string joinAdjuntos = String.Format(" JOIN(SELECT CATALOGO_{0}.ID_DOCUMENTO, COUNT(ADJUNTOS_C_{0}.ID_DOCUMENTO) AS TOTAL_ADJUNTOS FROM CATALOGO_{0} LEFT OUTER JOIN ADJUNTOS_C_{0} ON CATALOGO_{0}.ID_DOCUMENTO=ADJUNTOS_C_{0}.ID_DOCUMENTO WHERE FECHA_ELIMINACION IS NULL GROUP BY CATALOGO_{0}.ID_DOCUMENTO) T ON CATALOGO_{0}.ID_DOCUMENTO=T.ID_DOCUMENTO", idCatalogo);
            string joinFlujo = String.Format(" LEFT OUTER JOIN (SELECT DISTINCT CATALOGO_{0}.ID_DOCUMENTO,ID_FLUJO FROM CATALOGO_{0} LEFT OUTER JOIN FLUJO_CAT_{0} ON CATALOGO_{0}.ID_DOCUMENTO=FLUJO_CAT_{0}.ID_DOCUMENTO WHERE ID_FLUJO IS NOT NULL) F ON F.ID_DOCUMENTO=CATALOGO_{0}.ID_DOCUMENTO ", idCatalogo);
            Dictionary<string, object> parametrosBusquedaAmplia = _sqlCommons.GetParametros(campos, texto);
            int comenzarDesde = parametrosBusquedaAmplia.Count + 1;
            IList<Documento> resultado = new List<Documento>();

            string sqlSelect = String.Format("{0},T.TOTAL_ADJUNTOS,F.ID_FLUJO FROM CATALOGO_{1} {2} {3} {4} {5}", select, idCatalogo, joinAdjuntos, joinFlujo, whereAmplio, _sqlCommons.GetSqlPaginador());

            _sqlCommons.ExecuteQuery(sqlSelect, parametrosBusquedaAmplia, (error, reader) =>
            {
                if (!error)
                {
                    while (reader.Read())
                    {
                        IList<Campo> camposAux = new List<Campo>();

                        foreach (Campo campo in campos)
                        {
                            string nombreCampo = campo.Nombre;

                            if (reader[nombreCampo] != null)
                            {
                                camposAux.Add(new Campo()
                                {
                                    Id = campo.Id,
                                    Tipo = campo.Tipo,
                                    Valor = reader[nombreCampo].ToString(),
                                    Titulo = campo.Titulo,
                                    IdCatalogo = idCatalogo,
                                });
                            }
                        }
                        resultado.Add(new Documento()
                        {
                            Campos = camposAux,
                            Id = ParseLong(reader["ID_DOCUMENTO"].ToString()),
                            IdFlujo = ParseLong(reader["ID_FLUJO"].ToString()),
                            Numref = reader["NUMREF"].ToString(),
                            TotalAdjuntos = ParseUint(reader["TOTAL_ADJUNTOS"].ToString())
                        });
                    }
                }
            });

            return resultado;
        }

        /// <summary>
        /// Llenar el Documento con todos los datos
        /// </summary>
        /// <returns>Documentos con sus respectivos valores o null si hubo error</returns>
        public Documento GetDocumento(IList<Campo> campos, long idCatalogo, long idDocumento)
        {
            string sql = _sqlCommons.GetSelect(campos, idCatalogo);
            sql = String.Format("{0} WHERE ID_DOCUMENTO={1}", sql, idDocumento);

            Documento resultado = null;

            _sqlCommons.ExecuteQuery(sql, null, (error, reader) =>
            {
                if (!error)
                {
                    reader.Read();

                    IList<Campo> camposAux = new List<Campo>();

                    foreach (Campo campo in campos)
                    {
                        if (reader[campo.Nombre] != null)
                        {
                            camposAux.Add(new Campo()
                            {
                                Id = campo.Id,
                                IdCatalogo = idCatalogo,
                                Tipo = campo.Tipo,
                                Valor = reader[campo.Nombre].ToString(),
                                Titulo = campo.Titulo,
                                Tesauro = campo.Tesauro
                            });
                        }
                        resultado = new Documento()
                        {
                            Campos = camposAux,
                            Id = ParseLong(reader["ID_DOCUMENTO"].ToString()),
                            Numref = reader["NUMREF"].ToString()
                        };
                    }
                }
            });

            return resultado;
        }

        /// <summary>
        /// Trata de encontrar todos los documentos del Catálogo junto con los campos y sus respectivos
        /// valores, si los campos tienen algún valor se crea una consulta WHERE and con sus valores.
        /// Los resultados se regresan paginados dependiendo de la configuración del paginador en la dependencia
        /// ISQLCommonsScav
        /// </summary>
        /// <returns>Lista de Documentos con sus respectivos valores o una lista vacía si hubo error</returns>
        public IList<Documento> FindDocumentos(IList<Campo> campos, long idCatalogo)
        {
            string sql = _sqlCommons.GetSelect(campos, idCatalogo);
            string where = _sqlCommons.GetWhere(campos) + _sqlCommons.GetSqlPaginador();
            Dictionary<string, object> parametros = _sqlCommons.GetParametros(campos);

            IList<Documento> resultado = new List<Documento>();

            _sqlCommons.ExecuteQuery(sql + where, parametros, (error, reader) =>
            {
                if (!error)
                {
                    while (reader.Read())
                    {
                        IList<Campo> camposAux = new List<Campo>();

                        foreach (Campo campo in campos)
                        {
                            if (reader[campo.Nombre] != null)
                            {
                                camposAux.Add(new Campo()
                                {
                                    Id = campo.Id,
                                    IdCatalogo = idCatalogo,
                                    Tipo = campo.Tipo,
                                    Valor = reader[campo.Nombre].ToString(),
                                    Titulo = campo.Titulo
                                });
                            }
                        }
                        resultado.Add(new Documento()
                        {
                            Campos = camposAux,
                            Id = ParseLong(reader["ID_DOCUMENTO"].ToString()),
                            IdCatalogo = idCatalogo,
                            Numref = reader["NUMREF"].ToString()
                        });
                    }
                }
            });

            return resultado;
        }

        /// <summary>
        /// Retorna el total de registros de un catálogo de manera eficiente omitiendo la paginación
        /// </summary>
        public long GetTotal(long idCatalogo, IList<Campo> camposFiltro = null)
        {
            string select = String.Format("SELECT COUNT(*) AS TOTAL FROM CATALOGO_{0} ", idCatalogo);
            string where = "";

            if (camposFiltro != null)
            {
                where = _sqlCommons.GetWhere(camposFiltro);
            }

            Dictionary<string, object> parametros = _sqlCommons.GetParametros(camposFiltro);

            return _sqlCommons.ParseLong(_sqlCommons.ExecuteScalar(select + where, parametros).ToString());
        }

        /// <summary>
        /// Retorna el total de registros de un catálogo de manera eficiente omitiendo la paginación
        /// </summary>
        public long GetTotalBuscar(long idCatalogo, IList<Campo> camposFiltro)
        {
            string select = String.Format("SELECT COUNT(*) AS TOTAL FROM CATALOGO_{0} ", idCatalogo);
            string where = "";

            if (camposFiltro != null)
            {
                where = _sqlCommons.GetWhereLike(camposFiltro, GetOperadorLogicoBuscar(OperadorLogicoBuscar));
            }

            Dictionary<string, object> parametros = _sqlCommons.GetParametros(camposFiltro);

            return _sqlCommons.ParseLong(_sqlCommons.ExecuteScalar(select + where, parametros).ToString());
        }

        /// <summary>
        /// Retorna el total de registros de un catálogo de manera eficiente omitiendo la paginación
        /// </summary>
        public long GetTotalBusquedaAmplia(string texto, IList<Campo> campos, long idCatalogo)
        {
            string select = String.Format("SELECT COUNT(*) AS TOTAL FROM CATALOGO_{0} ", idCatalogo);
            string whereAmplio = _sqlCommons.GetWhereLike(campos, "OR", true, true);

            Dictionary<string, object> parametrosBusquedaAmplia = _sqlCommons.GetParametros(campos, texto);
            int comenzarDesde = parametrosBusquedaAmplia.Count + 1;

            string sqlSelect = String.Format("{0} {1}", select, whereAmplio);

            return _sqlCommons.ParseLong(_sqlCommons.ExecuteScalar(sqlSelect, parametrosBusquedaAmplia).ToString());
        }

        protected DateTime ParseDate(string str)
        {
            DateTime fecha = DateTime.Now;
            DateTime.TryParse(str, out fecha);
            return fecha;
        }

        protected long ParseLong(string number)
        {
            long num = 0;
            long.TryParse(number, out num);
            return num;
        }

        protected uint ParseUint(string number)
        {
            uint num = 0;
            uint.TryParse(number, out num);
            return num;
        }

        /// <summary>
        /// Establecer el Paginador
        /// </summary>
        public void SetPaginador(Paginador paginador)
        {
            _sqlCommons.Paginador = paginador;
        }

        /// <summary>
        /// Permite enviar un Flujo de Trabajo
        /// </summary>
        public bool EnviarFlujo(long idCatalogo, long idDocumento)
        {
            return _scavAdapter.EnviarFlujo(idCatalogo, idDocumento);
        }

        string GetOperadorLogicoBuscar(int operadorLogicoBuscar)
        {
            return operadorLogicoBuscar == 0 ? "AND" : "OR";
        }
    }
}