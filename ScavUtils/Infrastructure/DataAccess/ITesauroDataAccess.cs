﻿using ScavUtils.Infrastructure.Models;

namespace ScavUtils.Infrastructure.DataAccess
{
    public interface ITesauroDataAccess
    {
        /// <summary>
        /// Retorna los términos del Tesauro solicitado
        /// </summary>
        /// <param name="idTesauro">ID del Tesauro</param>
        /// <returns>Modelo Tesauro con la lista de términos</returns>
        Tesauro GetTerminos(long idTesauro);
    }
}
