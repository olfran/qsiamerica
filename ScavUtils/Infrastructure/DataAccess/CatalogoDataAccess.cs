﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace ScavUtils.Infrastructure.DataAccess
{
    public class CatalogoDataAccess : ICatalogoDataAccess
    {
        protected ISQLCommonsScav _sqlCommons;

        public CatalogoDataAccess(ISQLCommonsScav sqlCommons)
        {
            _sqlCommons = sqlCommons;
        }

        /// <summary>
        /// Devuelve una lista de los catálogos a los que un usuario tiene acceso
        /// </summary>
        /// <param name="idUsuario">Id del usuario</param>
        /// <returns>Lista de catálogos o lista vacía si hubo error o el usuario no tiene acceso a nada</returns>
        public IList<CatalogoPropiedades> GetCatalogos(long idUsuario)
        {
            IList<CatalogoPropiedades> resultado = new List<CatalogoPropiedades>();

            string sql = " SELECT DISTINCT CATALOGO.NOMBRE_CATALOGO, CATALOGO.ID_CATALOGO" +
                            " FROM GRUPO_USUARIO INNER JOIN" +
                            " GRUPO ON GRUPO_USUARIO.ID_GRUPO = GRUPO.ID_GRUPO RIGHT OUTER JOIN" +
                            " CATALOGO_ACCESO INNER JOIN" +
                            " CATALOGO ON CATALOGO_ACCESO.ID_CATALOGO = CATALOGO.ID_CATALOGO ON GRUPO.ID_GRUPO = CATALOGO_ACCESO.ID_GRUPO" +
                            " WHERE (CATALOGO_ACCESO.ID_USUARIO = " + idUsuario + ") OR" +
                            " (GRUPO_USUARIO.ID_USUARIO = " + idUsuario + ")" +
                            " ORDER BY CATALOGO.NOMBRE_CATALOGO";

            _sqlCommons.ExecuteQuery(sql, null, (error, reader) => {
                if (!error)
                {
                    IList<CatalogoPropiedades> lista = new List<CatalogoPropiedades>();

                    while (reader.Read())
                    {
                        resultado.Add(new CatalogoPropiedades()
                        {
                            Id = _sqlCommons.ParseLong(reader[1].ToString()),
                            Nombre = reader[0].ToString()
                        });

                    }
                }
            });

            return resultado;
        }

        /// <summary>
        /// Devuelve los diferentes tipos de directorios asociados al catálogo
        /// </summary>
        /// <param name="idCatalogo">Id catálogo</param>
        /// <returns>AdjuntoDirectorios</returns>
        public AdjuntoDirectorios GetDirectorios(long idCatalogo)
        {
            AdjuntoDirectorios resultado = new AdjuntoDirectorios();
            string sql = string.Format("SELECT TOP 1 RUTA_IMAGEN,RUTA_TEXTO,RUTA_MULTIMEDIA,RUTA_ARCHIVO,RUTA_WEB FROM CATALOGO WHERE ID_CATALOGO={0}", idCatalogo);

            _sqlCommons.ExecuteQuery(sql, null, (error, reader) =>
            {
                if (!error)
                {
                    IList<CatalogoPropiedades> lista = new List<CatalogoPropiedades>();

                    reader.Read();

                    resultado.Archivo = reader["RUTA_ARCHIVO"].ToString();
                    resultado.Imagen = reader["RUTA_IMAGEN"].ToString();
                    resultado.Texto = reader["RUTA_TEXTO"].ToString();
                    resultado.Multimedia = reader["RUTA_MULTIMEDIA"].ToString();
                    resultado.Web = reader["RUTA_WEB"].ToString();
                }
            });

            return resultado;
        }

        /// <summary>
        /// Verifica si un usuario tiene acceso al catálogo
        /// </summary>
        /// <param name="idUsuario">Id de usuario</param>
        /// <param name="idCatalogo">Id del catálogo a verificar</param>
        /// <returns>true si tiene acceso de lo contrario false</returns>
        public bool TieneAcceso(long idUsuario, long idCatalogo)
        {
            var catalogos = GetCatalogos(idUsuario);

            return catalogos.Any(x => x.Id == idCatalogo);
        }
    }
}
