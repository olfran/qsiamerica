﻿namespace ScavUtils.Infrastructure.Models
{
    /// <summary>
    /// Directorios de los diferentes tipos de adjuntos
    /// </summary>
    public class AdjuntoDirectorios
    {
        public string Imagen { get; set; }
        public string Texto { get; set; }
        public string Multimedia { get; set; }
        public string Archivo { get; set; }
        public string Web { get; set; }
    }
}
