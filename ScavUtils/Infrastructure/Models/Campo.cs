﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ScavUtils.Validators;
using System.Globalization;

namespace ScavUtils.Infrastructure.Models
{
    public class Campo
    {
        string _valor = "";
        int _tipoCampo;
        readonly Dictionary<string, int> DiccionarioTipoCampos = new Dictionary<string, int>()
        {
            { "@email", TipoCampo.Email },
            { "@clave", TipoCampo.Clave },
            { "@repiteclave campo_[1-9]+", TipoCampo.RepiteClave }
        };

        public long Id { get; set; }
        public long IdCatalogo { get; set; }
        public string Titulo { get; set; }
        public bool Unico { get; set; }

        public string Valor
        {
            get
            {
                //Si tiene valor por defecto y no es una fórmula
                if (!String.IsNullOrEmpty(ValorDefecto) && !ValorDefecto.StartsWith("@", StringComparison.InvariantCulture))
                {
                    return ValorDefecto;
                }

                if (Tipo == TipoCampo.Fecha)
                {
                    try
                    {
                        return DateTime.Parse(_valor).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        _valor = "";
                    }
                }

                return _valor;
            }
            set
            {
                _valor = value;
            }
        }

        public int Tipo
        {
            get
            {
                string formula = ValorDefecto.ToLower();

                foreach (var item in DiccionarioTipoCampos)
                {
                    if (Regex.IsMatch(formula, item.Key))
                    {
                        return DiccionarioTipoCampos[item.Key];
                    }
                }

                return _tipoCampo;
            }
            set
            {
                _tipoCampo = value;
            }
        }

        public string Nombre
        {
            get
            {
                if (Tipo == TipoCampo.Numref)
                {
                    return "NUMREF";
                }

                return string.Format("campo_{0}", Id);
            }
        }
        public bool Obligatorio { get; set; }
        public int LonguitudMaxima { get; set; }

        public string ValorDefecto { get; set; }
        //Tesauro
        public Tesauro Tesauro { get; set; }

        //Para validaciones
        public bool IsValid { get; set; }
        /// <summary>
        /// Opcional mensaje de error o de éxito
        /// </summary>
        public IList<string> Mensajes { get; private set; }
        /// <summary>
        /// Validadores del campo
        /// </summary>
        public IList<IValidator> Validators { get; set; }

        public Campo()
        {
            Titulo = "";
            Valor = "";
            Mensajes = new List<string>();
            Validators = new List<IValidator>();
            IsValid = true;
            ValorDefecto = "";
            Unico = false;
        }

        /// <summary>
        /// Validar el campo en base a todos los Validadores suministrados
        /// </summary>
        public bool Validate()
        {
            if (Validators != null)
            {
                IsValid = true;

                foreach (IValidator validator in Validators)
                {
                    if (!validator.IsValid(this))
                    {
                        IsValid = false;
                    }
                }
            }
            return IsValid;
        }
    }

    public struct TipoCampo
    {
        public const int Texto = 0;
        public const int Numero = 1;
        public const int Fecha = 2;
        public const int Memo = 3;
        public const int Imagen = 4;
        public const int Etiqueta = 5;
        public const int Numref = 8;
        public const int Vista = 10;
        public const int Email = 50;
        public const int Clave = 51;
        public const int RepiteClave = 52;
    };
}