﻿using System.Collections.Generic;

namespace ScavUtils.Infrastructure.Models
{
    /// <summary>
    /// Modelo para los Tesauros de SCAV
    /// </summary>
    public class Tesauro
    {
        public long ID { get; set; }
        public IList<Termino> Terminos { get; set; }

        public Tesauro()
        {
            Terminos = new List<Termino>();
        }
    }
}
