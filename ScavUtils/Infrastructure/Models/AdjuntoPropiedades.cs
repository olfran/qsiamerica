﻿namespace ScavUtils.Infrastructure.Models
{
    public class AdjuntoPropiedades
    {
        /// <summary>
        /// Id del archivo adjunto
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Nombre del archivo adjunto
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Nombre del archivo adjunto real
        /// </summary>
        public string NombreFisico { get; set; }

        /// <summary>
        /// Extensión del archivo
        /// </summary>
        public string Extension { get; set; }
    }
}