﻿using System.IO;
using System.IO.Compression;
using System.Text;

namespace Archicentro.Compresion
{
    /*
     * Permite comprimir o descomprimir datos con GZip
     * 
     * Por Olfran Jiménez <olfran@gmail.com>
     */

    /// <summary>
    /// Comprime o descomprime una entrada de datos sin pérdida
    /// utilizando el algoritmo Gzip
    /// </summary>
    public class GZip : ICompresion
    {
        /// <summary>
        /// Comprimir los datos suministrados utilizando Gzip
        /// </summary>
        /// <param name="entrada">Arreglo de bytes a comprimir</param>
        /// <returns>Datos comprimidos con Gzip</returns>
        public byte[] Comprimir(byte[] entrada)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (GZipStream gzipStream =
                    new GZipStream(memoryStream, CompressionMode.Compress))
                {
                    gzipStream.Write(entrada, 0, entrada.Length);
                }
                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Comprimir una cadena de caracteres con Gzip
        /// </summary>
        /// <param name="entrada">Cadena de caracteres a comprimir</param>
        /// <returns>Datos comprimidos con Gzip</returns>
        public byte[] ComprimirTexto(string entrada)
        {
            return Comprimir(Encoding.UTF8.GetBytes(entrada));
        }

        /// <summary>
        /// Descomprimir los datos suministrados
        /// </summary>
        /// <param name="entrada">Datos comprimidos con Gzip</param>
        /// <returns>Datos descomprimidos</returns>
        public byte[] Descomprimir(byte[] entrada)
        {
            using (MemoryStream memoryStream = new MemoryStream(entrada))
            {
                using (GZipStream gzipStream = new GZipStream(memoryStream, CompressionMode.Decompress, true))
                {
                    using (StreamReader reader = new StreamReader(gzipStream))
                    {
                        return Encoding.UTF8.GetBytes(reader.ReadToEnd());
                    }
                }
            }
        }

        /// <summary>
        /// Descomprimir los datos suministrados a cadena de caracteres
        /// </summary>
        /// <param name="entrada">Datos comprimidos con Gzip</param>
        /// <returns>Cadena de caracteres</returns>
        public string DescomprimirTexto(byte[] entrada)
        {
            return Encoding.UTF8.GetString(Descomprimir(entrada));
        }
    }
}