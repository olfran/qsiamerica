﻿namespace Archicentro.Compresion
{
    /*
     * Facilita el uso de las clases de compresión
     * 
     * Por Olfran Jiménez <olfran@gmail.com>
     */

    /// <summary>
    /// Facilita el uso de las clases que implementen ICompresion
    /// mediante inyección de dependencias en el constructor
    /// </summary>
    public class Compresor : ICompresion
    {
        ICompresion _compresor;

        /// <summary>
        /// Comprimir datos de un arreglo de bytes
        /// </summary>
        /// <returns>Datos comprimidos</returns>
        public Compresor(ICompresion compresor)
        {
            _compresor = compresor;
        }

        /// <summary>
        /// Establecer el compresor
        /// </summary>
        public ICompresion Estrategia
        {
            set { _compresor = value; }
        }

        /// <summary>
        /// Comprimir una cadena de caracteres
        /// </summary>
        /// <param name="entrada">Cadena de caracteres a comprimir</param>
        /// <returns>Datos comprimidos</returns>
        public byte[] Comprimir(byte[] entrada)
        {
            return _compresor.Comprimir(entrada);
        }

        /// <summary>
        /// Descomprimir los datos suministrados
        /// </summary>
        /// <param name="entrada">Datos comprimidos</param>
        /// <returns>Datos descomprimidos</returns>
        public byte[] ComprimirTexto(string entrada)
        {
            return _compresor.ComprimirTexto(entrada);
        }

        /// <summary>
        /// Descomprimir los datos suministrados
        /// </summary>
        /// <param name="entrada">Datos comprimidos</param>
        /// <returns>Datos descomprimidos</returns>
        public byte[] Descomprimir(byte[] entrada)
        {
            return _compresor.Descomprimir(entrada);
        }

        /// <summary>
        /// Descomprimir los datos suministrados a cadena de caracteres
        /// </summary>
        /// <param name="entrada">Datos comprimidos</param>
        /// <returns>Cadena de caracteres</returns>
        public string DescomprimirTexto(byte[] entrada)
        {
            return _compresor.DescomprimirTexto(entrada);
        }
    }
}