﻿namespace Archicentro.Compresion
{
    /// <summary>
    /// Comprime o descomprime una entrada de datos
    /// </summary>
    public interface ICompresion
    {
        /// <summary>
        /// Comprimir datos de un arreglo de bytes
        /// </summary>
        /// <param name="entrada">Arreglo de bytes a comprimir</param>
        /// <returns>Datos comprimidos</returns>
        byte[] Comprimir(byte[] entrada);
        /// <summary>
        /// Comprimir una cadena de caracteres
        /// </summary>
        /// <param name="entrada">Cadena de caracteres a comprimir</param>
        /// <returns>Datos comprimidos</returns>
        byte[] ComprimirTexto(string entrada);
        /// <summary>
        /// Descomprimir los datos suministrados
        /// </summary>
        /// <param name="entrada">Datos comprimidos</param>
        /// <returns>Datos descomprimidos</returns>
        byte[] Descomprimir(byte[] entrada);
        /// <summary>
        /// Descomprimir los datos suministrados a cadena de caracteres
        /// </summary>
        /// <param name="entrada">Datos comprimidos</param>
        /// <returns>Cadena de caracteres</returns>
        string DescomprimirTexto(byte[] entrada);
    }
}