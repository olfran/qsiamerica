﻿using System;

namespace Archicentro.Criptografia.Cifrado
{
    /*
     * Facilita el uso de las clases que implementan ICifrador
     * 
     * Por Olfran Jiménez <olfran@gmail.com>
     */

    /// <summary>
    /// Facilita el uso de las clases que implementen ICifrador
    /// proporcionando manejo de excepciones e información de la última excepción ocurrida
    /// </summary>
    public class Cifrador : ICifrador
    {
        ICifrador _cifrador;

        /// <summary>
        /// Última excepción que se genero dentro de la clase
        /// </summary>
        public Exception LastException { get; private set; }

        public Cifrador(ICifrador cifrador)
        {
            _cifrador = cifrador;
        }

        /// <summary>
        /// Establecer el cifrador
        /// </summary>
        public ICifrador Estrategia
        {
            set { _cifrador = value; }
        }

        /// <summary>
        /// Cifra datos
        /// </summary>
        /// <param name="entrada">Datos a cifrar</param>
        /// <param name="clave">Contraseña</param>
        /// <returns>Datos cifrados o null si hubo un error</returns>
        public byte[] Cifrar(byte[] entrada, string clave)
        {
            try
            {
                return _cifrador.Cifrar(entrada, clave);
            }
            catch(Exception ex)
            {
                LastException = ex;
                return null;
            }
        }

        /// <summary>
        /// Descifra datos
        /// </summary>
        /// <param name="entrada">Datos a descifrar</param>
        /// <param name="clave">Contraseña</param>
        /// <returns>Datos descifrados o null si hubo un error</returns>
        public byte[] Descifrar(byte[] entrada, string clave)
        {
            try
            {
                return _cifrador.Descifrar(entrada, clave);
            }
            catch (Exception ex)
            {
                LastException = ex;
                return null;
            }
        }
    }
}