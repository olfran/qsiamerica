﻿using Archicentro.Criptografia.Hash;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Archicentro.Criptografia.Cifrado
{
    /*
     * Cifrar o descifrar datos mediante el algoritmo AES
     * Retorna el texto cifrado en el siguiente formato:
     * [Bloque de n bits contraseña] donde n = longuitud del hash
     * [Bloque de n bits hash de verificación de los datos sin cifrar] donde n = longuitud del hash
     * [Resto de datos encriptados con AES y convertidos a Base64]
     * 
     * Por Olfran Jiménez <olfran@gmail.com>
     */

    /// <summary>
    /// Cifrar los archivos de exportación de catálogos
    ///</summary>
    public class CifradorExportacion
    {
        Cifrador _cifrador;
        Hasher _hasher;

        public enum Error { Ninguno, ErrorClave, ErrorHash, ErrorGeneral };

        /// <summary>
        /// Último error generado
        /// </summary>
        public Error LastError { get; private set; }
        
        public CifradorExportacion(Cifrador cifrador, Hasher hasher)
        {
            _cifrador = cifrador;
            _hasher = hasher;
        }

        /// <summary>
        /// Cifrar el texto de los archivos de exportación
        /// </summary>
        /// <param name="entrada">Texto en XML de exportación a cifrar</param>
        /// <param name="clave">Contraseña</param>
        /// <returns>Texto cifrado o null si hubo error</returns>
        public string Cifrar(string entrada, string clave)
        {
            try
            {
                byte[] hashClave = _hasher.HashByte(clave);
                byte[] hashEntrada = _hasher.HashByte(entrada);
                byte[] entradaCifrada = _cifrador.Cifrar(Encoding.UTF8.GetBytes(entrada), clave);
                                
                if (entradaCifrada == null || hashClave == null || hashEntrada == null)
                {
                    LastError = Error.ErrorGeneral;
                    return null;
                }

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    memoryStream.Write(hashClave, 0, hashClave.Length);
                    memoryStream.Write(hashEntrada, 0, hashEntrada.Length);
                    memoryStream.Write(entradaCifrada, 0, entradaCifrada.Length);
                    string resultado = Convert.ToBase64String(memoryStream.ToArray());

                    return resultado;
                }
            }
            catch (Exception)
            {
                LastError = Error.ErrorGeneral;
                return null;
            }
        }

        /// <summary>
        /// Descifrar el texto de los archivos de exportación
        /// </summary>
        /// <param name="entrada">Texto a descifrar</param>
        /// <param name="clave"></param>
        /// <returns></returns>
        public string Descifrar(string entrada, string clave)
        {
            try
            {
                byte[] entradaBytes = Convert.FromBase64String(entrada);

                //Verificar la clave
                byte[] hashClave = _hasher.HashByte(clave);
                byte[] claveEntrada = GetClaveEntrada(entradaBytes, hashClave.Length);

                if (!SonIguales(hashClave, claveEntrada))
                {
                    LastError = Error.ErrorClave;
                    return null;
                }

                byte[] hashTextoDescifrado = GetHashDatos(entradaBytes, hashClave.Length);
                byte[] textoCifrado = GetTextoCifrado(entradaBytes, hashClave.Length);
                byte[] descifrado = _cifrador.Descifrar(textoCifrado, clave);

                if (descifrado == null)
                {
                    LastError = Error.ErrorGeneral;
                    return null;
                }


                if (!SonIguales(hashTextoDescifrado, _hasher.HashByte(descifrado)))
                {
                    LastError = Error.ErrorHash;
                    return null;
                }

                return Encoding.UTF8.GetString(descifrado);
            }
            catch(Exception)
            {
                LastError = Error.ErrorGeneral;
                return null;
            }
        }

        byte[] GetClaveEntrada(byte[] entrada, int longuitudClave)
        {
            return entrada.Take(longuitudClave).ToArray();
        }

        byte[] GetHashDatos(byte[] entrada, int longuitudClave)
        {
            return entrada.Skip(longuitudClave).Take(longuitudClave).ToArray();
        }

        byte[] GetTextoCifrado(byte[] entrada, int longuitudClave)
        {
            return entrada.Skip(longuitudClave * 2).ToArray();
        }

        bool SonIguales(byte[] a, byte[] b)
        {
            if (a.Length != b.Length)
                return false;

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                    return false;                
            }
            return true;
        }
    }
}
