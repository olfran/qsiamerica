﻿namespace Archicentro.Criptografia.Cifrado
{
    /*
     * Permite cifrar o descifrar datos
     */
    /// <summary>
    /// Cifra o descifra datos
    /// </summary>
    public interface ICifrador
    {
        /// <summary>
        /// Cifra datos
        /// </summary>
        /// <param name="entrada">Datos a cifrar</param>
        /// <param name="clave">Contraseña</param>
        /// <returns>Datos cifrados</returns>
        byte[] Cifrar(byte[] entrada, string clave);

        /// <summary>
        /// Descifra datos
        /// </summary>
        /// <param name="entrada">Datos a descifrar</param>
        /// <param name="clave">Contraseña</param>
        /// <returns>Datos descifrados</returns>
        byte[] Descifrar(byte[] entrada, string clave);
    }
}
