﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Archicentro.Criptografia.Cifrado
{
    /*
     * Cifrar o descifrar datos mediante el algoritmo AES
     * http://es.wikipedia.org/wiki/Advanced_Encryption_Standard
     * 
     * Por Olfran Jiménez <olfran@gmail.com>
     */

    /// <summary>
    /// Cifra o descifra datos haciendo uso del algoritmo AES
    /// </summary>
    public class AES : ICifrador
    {
        byte[] _salt = { 0x26, 0xdc, 0xff, 0x00, 0xad, 0xed, 0x7a, 
                         0xee, 0xc5, 0xfe, 0x07, 0xaf, 0x4d, 0x08,
                         0x22, 0x3c };

        /// <summary>
        /// Cifra con AES y la clave especificada
        /// </summary>
        /// <param name="entrada">Datos a cifrar</param>
        /// <param name="clave">Contraseña</param>
        /// <returns>Datos cifrados</returns>
        public byte[] Cifrar(byte[] entrada, string clave)
        {
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(clave, _salt);
                aes.Key = pdb.GetBytes(32);
                aes.IV = pdb.GetBytes(16);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream =
                        new CryptoStream(memoryStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(entrada, 0, entrada.Length);
                    }
                    return memoryStream.ToArray();
                }
            }
        }

        /// <summary>
        /// Descifra los datos cifrados con AES
        /// </summary>
        /// <param name="entrada">Texto cifrado</param>
        /// <param name="clave">Contraseña</param>
        /// <returns>Texto descifrado o null si ocurrió un error (en la contraseña generalmente)</returns>
        public byte[] Descifrar(byte[] entrada, string clave)
        {
            try
            {
                using (Aes aes = Aes.Create())
                {
                    Rfc2898DeriveBytes password = new Rfc2898DeriveBytes(clave, _salt);
                    aes.Key = password.GetBytes(32);
                    aes.IV = password.GetBytes(16);
                    ICryptoTransform descifrador = aes.CreateDecryptor();

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (CryptoStream cryptoStream =
                            new CryptoStream(memoryStream, descifrador, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(entrada, 0, entrada.Length);
                        }
                        return memoryStream.ToArray();
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}