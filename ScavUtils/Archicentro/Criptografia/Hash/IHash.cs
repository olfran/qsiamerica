﻿namespace Archicentro.Criptografia.Hash
{
    /// <summary>
    /// Calcula el hash de los datos suministrados
    /// </summary>
    public interface IHash
    {
        /// <summary>
        /// Calcular el hash de los datos en byte[] suministrados
        /// </summary>
        /// <param name="datos">Datos a calcular</param>
        /// <returns>Hash de los datos suministrados</returns>
        string Hash(byte[] datos);

        /// <summary>
        /// Calcular el hash de la cadena de caracteres suministrada
        /// </summary>
        /// <param name="texto">Datos a calcular</param>
        /// <returns>Hash de los datos suministrados</returns>
        string Hash(string texto);

        /// <summary>
        /// Calcular el hash de los datos en byte[] suministrados
        /// </summary>
        /// <param name="datos">Datos a calcular</param>
        /// <returns>Hash de los datos suministrados en bytes</returns>
        byte[] HashByte(byte[] datos);

        /// <summary>
        /// Calcular el hash de la cadena de caracteres suministrada
        /// </summary>
        /// <param name="texto">Datos a calcular</param>
        /// <returns>Hash de los datos suministrados en bytes</returns>
        byte[] HashByte(string texto);
    }
}