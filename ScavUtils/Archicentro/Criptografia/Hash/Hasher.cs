﻿using System;
namespace Archicentro.Criptografia.Hash
{
    /*
     * Facilita el uso de las clases que implementan IHash
     * 
     * Por Olfran Jiménez <olfran@gmail.com>
     */

    /// <summary>
    /// Facilita el uso de las clases que implementen IHash
    /// proporcionando manejo de excepciones e información de la última excepción ocurrida
    /// </summary>
    public class Hasher
    {
        IHash _hasher;
        /// <summary>
        /// Última excepción que se genero dentro de la clase
        /// </summary>
        public Exception LastException { get; private set; }

        public Hasher(IHash hasher)
        {
            _hasher = hasher;
        }

        /// <summary>
        /// Establecer el hasher
        /// </summary>
        public IHash Estrategia
        {
            set { _hasher = value; }
        }

        /// <summary>
        /// Calcular el hash de los datos en byte[] suministrados
        /// </summary>
        /// <param name="datos">Datos a calcular</param>
        /// <returns>Hash de los datos suministrados o null si hubo error</returns>
        public string Hash(byte[] datos)
        {
            try
            {
                return _hasher.Hash(datos);
            }
            catch (Exception ex)
            {
                LastException = ex;
                return null;
            }
        }

        /// <summary>
        /// Calcular el hash de la cadena de caracteres suministrada
        /// </summary>
        /// <param name="texto">Datos a calcular</param>
        /// <returns>Hash de los datos suministrados o null si hubo error</returns>
        public string Hash(string texto)
        {
            try
            {
                return _hasher.Hash(texto);
            }
            catch (Exception ex)
            {
                LastException = ex;
                return null;
            }
        }


        public byte[] HashByte(byte[] datos)
        {
            return _hasher.HashByte(datos);
        }


        public byte[] HashByte(string texto)
        {
            return _hasher.HashByte(texto);
        }
    }
}