﻿using System.Text;
using NetSHA256 = System.Security.Cryptography.SHA256;

namespace Archicentro.Criptografia.Hash
{
    /*
     * Calcular el hash SHA256
     * http://es.wikipedia.org/wiki/Secure_Hash_Algorithm
     * 
     * Por Olfran Jiménez <olfran@gmail.com>
     */

    /// <summary>
    /// Calcula un hash de 256 bits mediante SHA256
    /// </summary>
    public class SHA256 : IHash
    {
        /// <summary>
        /// Permite obtener el SHA256 de los datos suministrados
        /// </summary>
        /// <param name="datos">Datos a calcular</param>
        /// <returns>Hash de 256 bits</returns>
        public string Hash(byte[] datos)
        {
            StringBuilder resultado = new StringBuilder();
            byte[] hashBytes = HashByte(datos);
            //Convertir a hexa
            foreach (byte b in hashBytes)
                resultado.AppendFormat("{0:x}", b);

            return resultado.ToString();
        }

        /// <summary>
        /// Permite obtener el SHA256 del texto
        /// </summary>
        /// <param name="texto">Texto a calcular</param>
        /// <returns>Hash de 256 bits</returns>
        public string Hash(string texto)
        {
            return Hash(Encoding.UTF8.GetBytes(texto));
        }

        public byte[] HashByte(byte[] datos)
        {
            using (NetSHA256 sha256 = NetSHA256.Create())
            {
                byte[] hashBytes = sha256.ComputeHash(datos);

                return hashBytes;
            }
        }

        public byte[] HashByte(string texto)
        {
            return HashByte(Encoding.UTF8.GetBytes(texto));
        }
    }
}