﻿using ScavUtils.Infrastructure.DataAccess.Adapters;
using ScavUtils.Infrastructure.Models;
using System;

namespace ScavUtils.Validators
{
    /// <summary>
    /// Valida campos Obligatorios
    /// </summary>
    public class Obligatorio : IValidator
    {
        /// <summary>
        /// Opcional mensaje de error o de éxito
        /// </summary>
        public string Mensaje {get; set; }

        /// <summary>
        /// Verificar si el campo es válido
        /// </summary>
        public bool IsValid(Campo campo)
        {
            bool resultado = !String.IsNullOrEmpty(campo.Valor);

            if (!resultado)
            {
                campo.Mensajes.Add(String.Format("El campo '{0}' es obligatorio", campo.Titulo));
            }
            
            return resultado;
        }
    }
}
