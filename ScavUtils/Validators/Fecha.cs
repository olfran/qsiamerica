﻿using ScavUtils.Infrastructure.Models;
using System;
using System.Text.RegularExpressions;

namespace ScavUtils.Validators
{
    /// <summary>
    /// Validar fechas con formato dd/mm/aaaa
    /// </summary>
    public class Fecha : IValidator
    {
        public bool IsValid(Campo campo)
        {
            string regex = @"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$";
            bool resultado = Regex.IsMatch(campo.Valor, regex);

            if (!resultado)
            {
                campo.Mensajes.Add("Por favor introduzca una fecha válida, el formato debe ser dd/MM/aaaa");
            }

            return resultado;
        }
    }
}
