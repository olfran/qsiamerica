﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;

namespace ScavUtils.Validators
{
    /// <summary>
    /// Factory para obtener los distintos validadores, dependiendo de las propiedades
    /// del campo campos suministrados
    /// </summary>
    public interface IValidatorFactory
    {
        void SetValidators(IList<Campo> campos);
    }
}
