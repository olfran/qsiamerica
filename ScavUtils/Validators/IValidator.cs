﻿using ScavUtils.Infrastructure.Models;

namespace ScavUtils.Validators
{
    public interface IValidator
    {
        /// <summary>
        /// Verificar si el campo es válido
        /// </summary>
        bool IsValid(Campo campo);
    }
}
