﻿using ScavUtils.Infrastructure.Models;
using System;
using System.Text.RegularExpressions;

namespace ScavUtils.Validators
{
    /// <summary>
    /// Valida Correos Electrónicos
    /// </summary>
    public class Email : IValidator
    {
        public bool IsValid(Campo campo)
        {
            bool resultado = true;

            if (!String.IsNullOrEmpty(campo.Valor))
            {
                string regex = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
                resultado = Regex.IsMatch(campo.Valor, regex);

                if (!resultado)
                {
                    campo.Mensajes.Add("Dirección de correo electrónico inválida");
                }
            }

            return resultado;
        }
    }
}
