﻿using ScavUtils.Infrastructure.Models;
using System.Collections.Generic;

namespace ScavUtils.Validators
{
    /// <summary>
    /// Factory para obtener los distintos validadores, dependiendo de las propiedades
    /// del campo campos suministrados
    /// </summary>
    public class DefaultValidatorFactory : IValidatorFactory
    {
        /// <summary>
        /// Campos
        /// </summary>
        public IList<Campo> Campos { get; set; }

        public void SetValidators(IList<Campo> campos)
        {
            foreach (Campo campo in campos)
            {
                campo.Validators = GetValidators(campo, campos);
            }
        }

        /// <summary>
        /// Retorna los validadores o null si no se cumple ninguna regla de negocio
        /// </summary>
        IList<IValidator> GetValidators(Campo campo, IList<Campo> campos)
        {
            IList<IValidator> resultado = new List<IValidator>();

            if (campo.Tipo == TipoCampo.Fecha)
            {
                resultado.Add(new Fecha());
            }

            if (campo.Tipo == TipoCampo.Numero)
            {
                resultado.Add(new Numero());
            }

            if (campo.Tipo == TipoCampo.Email)
            {
                resultado.Add(new Email());
            }

            if (campo.Tipo == TipoCampo.RepiteClave)
            {
                resultado.Add(new CompararClaves(campos));
            }

            if (campo.Obligatorio)
            {
                resultado.Add(new Obligatorio());
            }

            if (campo.LonguitudMaxima > 0)
            {
                resultado.Add(new LonguitudMaxima());
            }

            return resultado;
        }
    }
}
