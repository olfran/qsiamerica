﻿using ScavUtils.Infrastructure.Models;
using System;

namespace ScavUtils.Validators
{
    public class LonguitudMaxima : IValidator
    {
        public bool IsValid(Campo campo)
        {
            bool resultado = campo.Valor.Length <= campo.LonguitudMaxima;

            if (!resultado)
            {
                campo.Mensajes.Add(
                    String.Format("La longuitud máxima del campo '{0}' debe ser de {1} caracteres", 
                    campo.Titulo, campo.LonguitudMaxima));
            }

            return resultado;
        }
    }
}
