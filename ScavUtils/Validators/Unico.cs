﻿using ScavUtils.Infrastructure.DataAccess;
using ScavUtils.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ScavUtils.Validators
{
    /// <summary>
    /// Valida que el valor del campo no exista en el Catálogo, es decir, que sea único
    /// </summary>
    public class Unico : IValidator
    {
        private IScavDataAccess _dataAccess;

        public Unico(IScavDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public bool IsValid(Campo campo)
        {
            IList<Campo> camposABuscar = new List<Campo>()
            { 
                campo
            };

            IList<Documento> documentos = _dataAccess.FindDocumentos(camposABuscar, campo.IdCatalogo);

            //Si no encuentra ningún documento, quiere decir que no se repite
            bool resultado = documentos.Count == 0;

            if (!resultado)
            {
                campo.Mensajes.Add(String.Format("El campo '{0}' debe ser único, ese valor ya se encuentra registrado en nuestra Base de Datos", campo.Titulo));
            }

            return resultado;
        }
    }
}
