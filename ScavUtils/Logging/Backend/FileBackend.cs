﻿using System;
using System.IO;

namespace Logging.Backend
{
    /// <summary>
    /// Backend para almacenar logs en un archivo de texto. Es Thread Safe.
    /// Por Olfran Jiménez <olfran@gmail.com>
    /// </summary>
    public class FileBackend : ILogBackend
    {
        string _fileName, _logsFolderPath;
        public string FilePath { get; private set; }
        DateTime _now = DateTime.Now;
        //Locker para manejar concurrencia
        static object _locker = new object();

        /// <summary>
        /// Constructor que acepta el path completo del archivo
        /// </summary>
        /// <param name="filePath">Path completo del archivo</param>
        public FileBackend(string filePath)
        {
            FilePath = filePath;
        }

        /// <summary>
        /// Constructor que acepta el nombre de la carpeta y genera el nombre del archivo automaticamente
        /// según el día Ej. 01-01-2016.txt o 1326549312.txt
        /// </summary>
        /// <param name="logsFolderPath">Nombre de la carpeta</param>
        /// <param name="useDayAsFileName">Generar el nombre del archivo en base al día, si es false genera un timestamp</param>
        public FileBackend(string logsFolderPath, bool useDayAsFileName)
        {
            if (useDayAsFileName)
            {
                _fileName = String.Format("{0}.txt", _now.ToString("dd-MM-yyyy"));
            }
            else
            {
                _fileName = String.Format("{0}.txt", DateTime.Now.ToFileTime());
            }
            _logsFolderPath = Path.Combine(logsFolderPath, _now.Year.ToString());
            FilePath = Path.Combine(_logsFolderPath, _fileName);
        }

        /// <summary>
        /// Constructor que acepta el nombre y la carpeta donde se almacenan los Logs
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <param name="logFolderPath">Nombre de la carpeta</param>
        public FileBackend(string fileName, string logFolderPath)
        {
            _fileName = fileName;
            _logsFolderPath = logFolderPath;
            FilePath = Path.Combine(_logsFolderPath, fileName);
        }

        /// <summary>
        /// Escribir el log al archivo
        /// </summary>
        /// <param name="log">Mensaje</param>
        public bool Log(string log)
        {
            try
            {
                lock (_locker)
                {
                    CreateFileIfNotExists();

                    using (FileStream fileStream = new FileStream(FilePath, FileMode.Append))
                    {
                        using (StreamWriter writer = new StreamWriter(fileStream))
                        {
                            DateTime now = DateTime.Now;
                            writer.WriteLine(String.Format("{0} - {1}", now.ToLongDateString(), now.ToLongTimeString()));
                            writer.WriteLine(String.Format("{0}", log));
                            writer.WriteLine("");
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Crear el directorio y el archivo si no existe
        /// </summary>
        private void CreateFileIfNotExists()
        {
            if (!String.IsNullOrEmpty(_logsFolderPath))
            {
                Directory.CreateDirectory(_logsFolderPath);
            }
            if (!File.Exists(FilePath))
            {
                File.CreateText(FilePath).Dispose();
            }
        }
    }
}