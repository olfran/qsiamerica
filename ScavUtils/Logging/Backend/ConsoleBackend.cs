﻿using System;

namespace Logging.Backend
{
    public class ConsoleBackend : ILogBackend
    {
        public bool Log(string log)
        {
            Console.WriteLine(log);
            return true;
        }
    }
}