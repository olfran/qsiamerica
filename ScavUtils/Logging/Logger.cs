﻿using Logging.Backend;

namespace Logging
{
    /// <summary>
    /// Clase para realizar logs simples, requiere de un Backend
    /// a través de inyección de dependencias, lo cual permite
    /// personalizar la salida creando distintas clases a partir
    /// de la interfaz ILogBackend
    /// 
    /// Por Olfran Jiménez <olfran@gmail.com>
    /// </summary>
    public class Logger : ILogger
    {
        ILogBackend _logBackend;
        
        public Logger(ILogBackend logBackend)
        {
            _logBackend = logBackend;
        }

        public bool Log(string message)
        {
            return _logBackend.Log(message);
        }
    }
}