﻿using ScavConfiguration.Interfaces;

namespace ScavConfiguration
{
    /// <summary>
    /// Clase que permite leer del archivo de configuraciones de SCAV
    /// necesita de una implementación de IScavConfigurationReader
    /// la cual es pasada en el constructor
    /// 
    /// Por Olfran Jiménez <olfran@gmail.com>
    /// </summary>
    public class ScavConfigurationManager : IScavConfigurationManager
    {
        private IScavConfigurationReader _configurationReader;

        public ScavConfigurationManager(IScavConfigurationReader configurationReader)
        {
            _configurationReader = configurationReader;
        }

        public string Get(string itemName)
        {
            return _configurationReader.Get(itemName);
        }
    }
}