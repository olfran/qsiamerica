﻿using System;
using System.Linq;
using System.Xml.Linq;
using ScavConfiguration.Interfaces;

namespace ScavConfiguration.Readers
{
    /// <summary>
    /// Clase que permite los datos del applicationSettings del archivo de
    /// configuraciones de SCAV
    /// </summary>
    public class ScavWebConfigurationReader : IScavConfigurationReader
    {
        string _pathWebConfig;

        XDocument _xDocument;

        XDocument XmlDocument
        {
            get
            {
                if (_xDocument == null)
                {
                    _xDocument = XDocument.Load(_pathWebConfig);
                }
                return _xDocument;
            }
        }

        public ScavWebConfigurationReader(string pathWebConfig)
        {
            _pathWebConfig = pathWebConfig;
        }

        public string Get(string itemName)
        {
            try
            {
                return XmlDocument.Element("configuration")
                        .Element("applicationSettings")
                        .Element("scav4.Properties.Settings")
                        .Elements("setting")
                        .Where(x => x.Attribute("name").Value.Equals(itemName))
                        .FirstOrDefault().Value;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}

