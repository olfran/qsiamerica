# Integración del blog de QSIAmerica mediante Web Services
### Por Olfran Jiménez <olfran@gmail.com>

##Descripción
Integración de una sección del blog de QSIAmerica en Wordpress con AIRE/SCAV mediante servicios web.

## Herramientas/Tecnologías utilizadas
* .NET Framework 4.5
* ASP.NET MVC 4
* SOLID
* Programming to Interfaces
* Multilayered